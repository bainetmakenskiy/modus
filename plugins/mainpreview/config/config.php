<?php
/*-------------------------------------------------------
*
*   LiveStreet Engine Social Networking
*   Copyright © 2008 Mzhelskiy Maxim
*
*--------------------------------------------------------
*
*   Official site: www.livestreet.ru
*   Contact e-mail: rus.engine@gmail.com
*
*   GNU General Public License, version 2:
*   http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
*
---------------------------------------------------------
*/

/**
 * Адрес конвертера превью
 */
Config::Set('router.page.mainpreview', 'PluginMainpreview_ActionMain');

/**
 * Настройки
 */
$config['make_preview_video']   = true;  // Создавать или нет автоматические превью топика на основе вставленного в текст видео
$config['make_preview_image']   = true;  // Создавать или нет автоматические превью топика на основе вставленного в текст изображения
$config['preview_minimal_size_width']   = 1200;  // Минимальная ширина превью, если изображение меньше, то на его основе превью создаваться не будет. Актуально только для автоматических превью
$config['preview_minimal_size_height']  = 375;  // Минимальная высота превью, если изображение меньше, то на его основе превью создаваться не будет. Актуально только для автоматических превью

/**
 * Список размеров превью топика
 * Обычно задается другим плагином или шаблоном
 */
$config['size_images_preview']=array(
	
	// Большая картинка
	array(
		'w' => 1200,
		'h' => null,
		'crop' => false,
	),

	// Для слайдера топ
	array(
		'w' => 1200,
		'h' => 375,
		'crop' => true,
	),

	// Для топиков в списке с 960px
	array(
		'w' => 770,
		'h' => 300,
		'crop' => true,
	),

	// Для топиков в списке с 1200px
	array(
		'w' => 870,
		'h' => 300,
		'crop' => true,
	),

	// Для топиков theme2 и theme3
	array(
		'w' => 470,
		'h' => 270,
		'crop' => true,
	),

	// Для топиков theme4
	array(
		'w' => 420,
		'h' => 420,
		'crop' => true,
	),

	array(
		'w' => 260,
		'h' => 270,
		'crop' => true,
	),

	// В слайдер на главной
	array(
		'w' => 200,
		'h' => 150,
		'crop' => true,
	),

	// Малениькие в футер
	array(
		'w' => 70,
		'h' => 70,
		'crop' => true,
	)

	
);

/**
 * Системный параметр, его НЕ нужно изменять
 */
$config['load']   = true;

return $config;
?>