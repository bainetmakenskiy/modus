<p class="topic-edit">
	{if $oTopicEdit and $oTopicEdit->getPreviewImage()}
		<img src="{$oTopicEdit->getPreviewImageWebPath(470crop)}" alt="" class="full-with" />
		<label for="topic_preview_image_delete"><input type="checkbox" id="topic_preview_image_delete" name="topic_preview_image_delete" value="on" class="input-checkbox"> &mdash; {$aLang.plugin.mainpreview.form_preview_image_delete} {if $oTopicEdit->getPreviewImageIsAuto()}({$aLang.plugin.mainpreview.form_preview_image_delete_auto}){/if}</label>
	{/if}
	<label for="topic_preview_image">{$aLang.plugin.mainpreview.form_preview_image}</label>
	<input class="input-200" type="file" name="topic_preview_image" id="topic_preview_image">
	<span class="note">{$aLang.topic_add_photoset_note}</span>
</p>