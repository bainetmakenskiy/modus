<?php
/*-------------------------------------------------------
*
*   LiveStreet Engine Social Networking
*   Copyright © 2008 Mzhelskiy Maxim
*
*--------------------------------------------------------
*
*   Official site: www.livestreet.ru
*   Contact e-mail: rus.engine@gmail.com
*
*   GNU General Public License, version 2:
*   http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
*
---------------------------------------------------------
*/

/**
 * Настройки
 */
Config::Set('module.topic.per_page',9);	// Число топиков на одну страницу
$config['count_top_users']   = 0;  // Число ТОП пользователей. Внимание: функция пока не используется.
$config['count_top_topics']   = 3;  // Число топиков в верхнем ТОП
$config['topic_time_top']   = 60*60*24*7*10*10;  // Число секунд за которые считать ТОП топиков, по дефолту стоит 7 дней
$config['count_profile_topics']   = 12;  // Число топиков на странице профиля пользователя
$config['count_profile_comments']   = 10;  // Число комментов на странице профиля пользователя

$config['preview_size_w']=1200;	// Ширина
$config['preview_size_h']=375;	// Высота, при crop=false используется как минимально возможная высота
$config['preview_crop']=true;	// Делать из картинки кроп? false - если не нужно обрезать картинки по высоте
$config['preview_big_size_w']=1200;	// Ширина большого варианта
$config['preview_big_size_h']=375;	// Высота большого варианта, при crop=false используется как минимально возможная высота
$config['preview_big_crop']=false;	// Делать из картинки кроп для большого варианта? false - если не нужно обрезать картинки по высоте


/***************************************************************************************************
 ********************************          НИЖЕ НЕ ТРОГАТЬ!         ********************************
 ***************************************************************************************************
 */

$config['size_images_preview']=array(
	array(
		'w' => $config['preview_size_w'],
		'h' => $config['preview_crop'] ? $config['preview_size_h'] : null,
		'crop' => $config['preview_crop'],
	),
	array(
		'w' => $config['preview_big_size_w'],
		'h' => $config['preview_big_crop'] ? $config['preview_big_size_h'] : null,
		'crop' => $config['preview_big_crop'],
	)
);

return $config;
?>