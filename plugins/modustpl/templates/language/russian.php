<?php
/*-------------------------------------------------------
*
*   LiveStreet Engine Social Networking
*   Copyright © 2008 Mzhelskiy Maxim
*
*--------------------------------------------------------
*
*   Official site: www.livestreet.ru
*   Contact e-mail: rus.engine@gmail.com
*
*   GNU General Public License, version 2:
*   http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
*
---------------------------------------------------------
*/

/**
 * Русский языковой файл плагина
 */
return array(
	'modustpl_admin_list_convert' => 'Провести конвертацию фото-сетов для шаблона modus',
	'modustpl_submit_convert' => 'Запустить конвертацию размеров фото-сетов',
	'modustpl_submit_convert_notice' => 'Внимание! Если у вас много фото-сетов, то процесс конвертации может занять длительное время. ',
	'modustpl_convert_end' => 'Проверено фото-сетов %%count_topic%%, конвертированно фотографий %%count_photo%%',
	'modustpl_form_preview_image_delete' => 'Удалить превью',
	'modustpl_form_preview_image_delete_auto' => 'автоматическое',
	'modustpl_form_preview_image' => 'Превью топика',
	'modustpl_form_preview_image_notice' => 'Вы можете загрузить картинку, которая будет превью топика на главной странице',
);

?>