			<li class="pages">
            	<a href="#" class="bt-dropdown-pages">
              		<div class="txt">Еще</div>
              		<div class="ar"><i class="fa fa-chevron-down"></i></div>
            	</a>
            	<div class="dropdown-pages">
                	<div class="dropdown-line"></div>
                  	<ul>
                  		{foreach from=$aPagesMain item=oPage}
                      		<li {if $sAction=='page' and $sEvent==$oPage->getUrl()}class="active"{/if}><a href="{router page='page'}{$oPage->getUrlFull()}/">{$oPage->getTitle()}</a></li>
                    	{/foreach}
                  	</ul>
            	</div>
          	</li>