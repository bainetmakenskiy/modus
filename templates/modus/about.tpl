<section class="wrapper">
    <div class="container about">

        <div class="title text_align_center">
          <h1>{$aLang.modus_block_about_index_title}</h1>
          <p>{$aLang.modus_block_about_index_title_text}</p>
        </div>
<div class="why-img">
        <div class="brouser-left width-3 border-radius">
          <div class="brouser-panel">
            {* Текстовые кнопки над картинкой *}
            <ul class="clearfix">
              <li></li>
              <li></li>
              <li></li>
              <li class="minus"></li>
            </ul>
          </div>
          <img src="{cfg name='path.static.skin'}/images/about_left.jpg" alt="" class="img-rounded full-with" />
        </div>
        <div class="brouser-center width-6 border-radius">
          <div class="brouser-panel">
            {* Текстовые кнопки над картинкой *}
            <ul class="clearfix">
              <li></li>
              <li></li>
              <li></li>
              <li class="minus"></li>
            </ul>
          </div>
          <img src="{cfg name='path.static.skin'}/images/about_center.jpg" alt="" class="img-rounded full-with" />
        </div>
        <div class="brouser-right width-3 border-radius">
          <div class="brouser-panel">
            {* Текстовые кнопки над картинкой *}
            <ul class="clearfix">
              <li></li>
              <li></li>
              <li></li>
              <li class="minus"></li>
            </ul>
          </div>
          <img src="{cfg name='path.static.skin'}/images/about_right.jpg" alt="" class="img-rounded full-with align_right" />
        </div>
</div>
    </div>
</section>

<section class="wrapper">
    <div class="container about-more">
        <div class="width-3">
            <h4>{$aLang.modus_block_about_index_function_title}</h4>
            <ul class="arrow">
                <li>{$aLang.modus_block_about_index_function_li_1}</li>
                <li>{$aLang.modus_block_about_index_function_li_2}</li>
                <li>{$aLang.modus_block_about_index_function_li_3}</li>
                <li>{$aLang.modus_block_about_index_function_li_4}</li>
                <li>{$aLang.modus_block_about_index_function_li_5}</li>
                <li>{$aLang.modus_block_about_index_function_li_6}</li>
                <li><a href="#" class="js-infobox" title="Читать полный список">{$aLang.modus_block_about_index_function_li_7}</a></li>
            </ul>
        </div>
        <div class="width-6">
            <p>{$aLang.modus_block_about_index_text_rating}</p>
           
            <div class="knob-bg width-2 js-infobox" title="{$aLang.modus_block_about_index_title_tooltip_rating_1}">
              <input class="knob" data-fgColor="{if {cfg name='view.theme'} == 'green_orange'}#e36b2c{elseif {cfg name='view.theme'} == 'purpur_blue'}#20a8da{else}#e2534b{/if}" data-bgColor="{if {cfg name='view.theme'} == 'green_orange'}#70c570{elseif {cfg name='view.theme'} == 'purpur_blue'}#a65980{else}#76c7c0{/if}" data-width="110" data-angleOffset="90" data-thickness=".25" data-readOnly=true value="{cfg name='view.progress.n1'}">
              <div class="title">{$aLang.modus_block_about_index_title_rating_1}</div>
            </div>
            <div class="knob-bg width-2 js-infobox" title="{$aLang.modus_block_about_index_title_tooltip_rating_2}">
              <input class="knob" data-fgColor="{if {cfg name='view.theme'} == 'green_orange'}#e36b2c{elseif {cfg name='view.theme'} == 'purpur_blue'}#20a8da{else}#e2534b{/if}" data-bgColor="{if {cfg name='view.theme'} == 'green_orange'}#70c570{elseif {cfg name='view.theme'} == 'purpur_blue'}#a65980{else}#76c7c0{/if}" data-width="110" data-angleOffset="90" data-thickness=".25" data-readOnly=true value="{cfg name='view.progress.n2'}">
              <div class="title">{$aLang.modus_block_about_index_title_rating_2}</div>
            </div>
            <div class="knob-bg width-2 js-infobox" title="{$aLang.modus_block_about_index_title_tooltip_rating_3}">
              <input class="knob" data-fgColor="{if {cfg name='view.theme'} == 'green_orange'}#e36b2c{elseif {cfg name='view.theme'} == 'purpur_blue'}#20a8da{else}#e2534b{/if}" data-bgColor="{if {cfg name='view.theme'} == 'green_orange'}#70c570{elseif {cfg name='view.theme'} == 'purpur_blue'}#a65980{else}#76c7c0{/if}" data-width="110" data-angleOffset="90" data-thickness=".25" data-readOnly=true value="{cfg name='view.progress.n3'}">
              <div class="title">{$aLang.modus_block_about_index_title_rating_3}</div>
            </div>
            <div class="knob-bg width-2 js-infobox" title="{$aLang.modus_block_about_index_title_tooltip_rating_4}">
              <input class="knob" data-fgColor="{if {cfg name='view.theme'} == 'green_orange'}#e36b2c{elseif {cfg name='view.theme'} == 'purpur_blue'}#20a8da{else}#e2534b{/if}" data-bgColor="{if {cfg name='view.theme'} == 'green_orange'}#70c570{elseif {cfg name='view.theme'} == 'purpur_blue'}#a65980{else}#76c7c0{/if}" data-width="110" data-angleOffset="90" data-thickness=".25" data-readOnly=true value="{cfg name='view.progress.n4'}">
              <div class="title">{$aLang.modus_block_about_index_title_rating_4}</div>
            </div>

        </div>

        <div class="width-3">
            <h4>{$aLang.modus_block_about_index_say_title}</h4>

            <p class="say">{$aLang.modus_block_about_index_say_1}</p>
            <a href="#" class="autor">{$aLang.modus_block_about_index_say_name_1}</a>

            <p class="say">{$aLang.modus_block_about_index_say_2}</p>
            <a href="#" class="autor">{$aLang.modus_block_about_index_say_name_2}</a>

        </div>
    </div>
</section>
