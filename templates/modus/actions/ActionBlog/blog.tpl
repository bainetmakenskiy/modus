{include file='header.tpl'}
{assign var="oUserOwner" value=$oBlog->getOwner()}
{assign var="oVote" value=$oBlog->getVote()}


<script type="text/javascript">
	jQuery(function($){
		ls.lang.load({lang_load name="blog_fold_info,blog_expand_info"});
	});
</script>

{if $oUserCurrent and $oUserCurrent->isAdministrator()}
	<div id="blog_delete_form" class="modal">
		<header class="modal-header">
			<h3>{$aLang.blog_admin_delete_title}</h3>
			<a href="#" class="close jqmClose"></a>
		</header>
		
		
		<form action="{router page='blog'}delete/{$oBlog->getId()}/" method="POST" class="modal-content">
			<p><label for="topic_move_to">{$aLang.blog_admin_delete_move}:</label>
			<select name="topic_move_to" id="topic_move_to" class="input-width-full">
				<option value="-1">{$aLang.blog_delete_clear}</option>
				{if $aBlogs}
					<optgroup label="{$aLang.blogs}">
						{foreach from=$aBlogs item=oBlogDelete}
							<option value="{$oBlogDelete->getId()}">{$oBlogDelete->getTitle()|escape:'html'}</option>
						{/foreach}
					</optgroup>
				{/if}
			</select></p>
			
			<input type="hidden" value="{$LIVESTREET_SECURITY_KEY}" name="security_ls_key" />
			<button type="submit" class="button button-primary">{$aLang.blog_delete}</button>
		</form>
	</div>
{/if}


		<section class="wrapper gray-light">
  			<div class="container sup-menu">
      			<div class="title align_left">
        			<a href="#" id="blog-more" onclick="return ls.blog.toggleInfo()">{$aLang.blog_expand_info}</a>
        			<div class="vote-blog">
        				<a href="#" onclick="return ls.vote.vote({$oBlog->getId()},this,1,'blog');">{$aLang.modus_menu_sup_plus}</a>
        				<span id="vote_total_blog_{$oBlog->getId()}" class="js-infobox" title="{$aLang.blog_vote_count}: {$oBlog->getCountVote()}">{if $oBlog->getRating() > 0}+{/if}{$oBlog->getRating()}</span>
        				<a href="#" onclick="return ls.vote.vote({$oBlog->getId()},this,-1,'blog');">{$aLang.modus_menu_sup_minus}</a>
        			</div>
      			</div>
      			<div class="nav align_right">

					<ul>
						<li>
							<a href="{router page='rss'}blog/{$oBlog->getUrl()}/" class="rss">RSS</a> / 
						</li>
							
							{if $oUserCurrent and ($oUserCurrent->getId()==$oBlog->getOwnerId() or $oUserCurrent->isAdministrator() or $oBlog->getUserIsAdministrator() )}
								<li>
									<a href="{router page='blog'}edit/{$oBlog->getId()}/" title="{$aLang.blog_edit}" class="edit">{$aLang.blog_edit}</a> / 
								</li>
								{if $oUserCurrent->isAdministrator()}
									<li>
										<a href="#" title="{$aLang.blog_delete}" id="blog_delete_show" class="delete">{$aLang.blog_delete}</a> / 
									</li>
								{else}
									<li>
										<a href="{router page='blog'}delete/{$oBlog->getId()}/?security_ls_key={$LIVESTREET_SECURITY_KEY}" title="{$aLang.blog_delete}" onclick="return confirm('{$aLang.blog_admin_delete_confirm}');" >{$aLang.blog_delete}</a> / 
									</li>
								{/if}
									
							{/if}

						<li><a href="{router page='personal_blog'}" {if $sMenuSubItemSelect=='good'}class="active"{/if}>{$aLang.blog_menu_personal_good}</a> / </li>
						<li><a href="{$sMenuSubBlogUrl}" {if $sMenuSubItemSelect=='good'}class="active"{/if}>{$aLang.blog_menu_collective_good}</a> / </li>
						<li><a href="{$sMenuSubBlogUrl}newall/" {if $sMenuSubItemSelect=='new'}class="active"{/if}>{$aLang.blog_menu_collective_new} {if $iCountTopicsBlogNew>0} +{$iCountTopicsBlogNew}{/if}</a> / </li>
						<li><a href="{$sMenuSubBlogUrl}discussed/" {if $sMenuSubItemSelect=='discussed'}class="active"{/if}>{$aLang.blog_menu_collective_discussed}</a> / </li>
						<li><a href="{$sMenuSubBlogUrl}top/" {if $sMenuSubItemSelect=='top'}class="active"{/if}>{$aLang.blog_menu_collective_top}</a> / </li>
						{hook run='menu_blog_blog_item'}
					</ul>

        			
      			</div>
      			<div class="triangle-down"></div>
  			</div>
		</section>

		<section class="wrapper white blogs info-block">
		<div class="container info border-radius">
  		
  			<div class="text {if $oUserCurrent and $oUserCurrent->getId()!=$oBlog->getOwnerId()}width-9{/if}">
    			<h1>{if $oBlog->getType()=='close'}<i class="fa fa-key js-infobox" title="{$aLang.blog_closed}"></i> {/if}{$oBlog->getTitle()|escape:'html'}</h1>
    			<p>{$oBlog->getDescription()}</p>
  			</div>
  			<div class="width-3">
  				{if $oUserCurrent and $oUserCurrent->getId()!=$oBlog->getOwnerId()}
    				<a href="#" class="button align_right js-infobox" onclick="ls.blog.toggleJoin(this,{$oBlog->getId()}); return false;">{if $oBlog->getUserIsJoin()}{$aLang.blog_leave}{else}{$aLang.blog_join}{/if}</a>
    			{/if}
  			</div>

  			<div class="blog-more-content" id="blog-more-content" style="display: none;">
				
			  <div class="blog-footer width-12">
				<p>
				{hook run='blog_info_begin' oBlog=$oBlog}
					<strong>{$aLang.blog_user_administrators} ({$iCountBlogAdministrators}):</strong>							
						<a href="{$oUserOwner->getUserWebPath()}" class="user"><i class="icon-user"></i>
						{if {cfg name='view.login.name'} == '1'}
							{if $oUserOwner->getProfileName()}
                                {$oUserOwner->getProfileName()|escape:'html'}
                          	{else}
                                {$oUserOwner->getLogin()}
                          	{/if}
                        {else}
                        	{$oUserOwner->getLogin()}
                        {/if}
						</a>
						{if $aBlogAdministrators}			
							{foreach from=$aBlogAdministrators item=oBlogUser}
								{assign var="oUser" value=$oBlogUser->getUser()}  									
									<a href="{$oUser->getUserWebPath()}" class="user"><i class="icon-user"></i>
										{if {cfg name='view.login.name'} == '1'}
                          					{if $oUser->getProfileName()}
                                				{$oUser->getProfileName()|escape:'html'}
                          					{else}
                                				{$oUser->getLogin()}
                          					{/if}
                      					{else}
                                			{$oUser->getLogin()}
                      					{/if}
									</a>
							{/foreach}	
						{/if}<br />		

			
					<strong>{$aLang.blog_user_moderators} ({$iCountBlogModerators}):</strong>
						{if $aBlogModerators}						
							{foreach from=$aBlogModerators item=oBlogUser}  
								{assign var="oUser" value=$oBlogUser->getUser()}									
									<a href="{$oUser->getUserWebPath()}" class="user"><i class="icon-user"></i>
										{if {cfg name='view.login.name'} == '1'}
                          					{if $oUser->getProfileName()}
                                				{$oUser->getProfileName()|escape:'html'}
                          					{else}
                                				{$oUser->getLogin()}
                          					{/if}
                      					{else}
                                			{$oUser->getLogin()}
                      					{/if}
									</a>
							{/foreach}							
						{else}
							{$aLang.blog_user_moderators_empty}
						{/if}<br />
			
			
						<strong>{$aLang.blog_user_readers} ({$iCountBlogUsers}):</strong>
						{if $aBlogUsers}
							{foreach from=$aBlogUsers item=oBlogUser}
								{assign var="oUser" value=$oBlogUser->getUser()}
									<a href="{$oUser->getUserWebPath()}" class="user"><i class="icon-user"></i>
										{if {cfg name='view.login.name'} == '1'}
                          					{if $oUser->getProfileName()}
                                				{$oUser->getProfileName()|escape:'html'}
                          					{else}
                                				{$oUser->getLogin()}
                          					{/if}
                      					{else}
                                			{$oUser->getLogin()}
                      					{/if}
									</a>
							{/foreach}
				
						{if count($aBlogUsers) < $iCountBlogUsers}
							<br /><a href="{$oBlog->getUrlFull()}users/">{$aLang.blog_user_readers_all}</a>
						{/if}
				{else}
					{$aLang.blog_user_readers_empty}
				{/if}
				{hook run='blog_info_end' oBlog=$oBlog}
				</p>
			  </div>
			</div>
  		
		</div>
		</section>

{hook run='blog_info' oBlog=$oBlog}



{if $bCloseBlog}
	<section class="wrapper white">
  			<div class="container error">
				{$aLang.blog_close_show}
			</div>
		</section>
{else}
	{include file='topic_list.tpl'}
{/if}


{include file='footer.tpl'}