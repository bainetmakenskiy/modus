{include file='header.tpl' menu='blog'}

<section class="wrapper white">
    <div class="container">
    	<section class="content two-thirds">

{include file='topic.tpl'}
{include 
	file='comment_tree.tpl' 	
	iTargetId=$oTopic->getId()
	iAuthorId=$oTopic->getUserId()
	sAuthorNotice=$aLang.topic_author
	sTargetType='topic'
	iCountComment=$oTopic->getCountComment()
	sDateReadLast=$oTopic->getDateRead()
	bAllowNewComment=$oTopic->getForbidComment()
	sNoticeNotAllow=$aLang.topic_comment_notallow
	sNoticeCommentAdd=$aLang.topic_comment_add
	bAllowSubscribe=true
	oSubscribeComment=$oTopic->getSubscribeNewComment()
	aPagingCmt=$aPagingCmt}

		</section>

		{if !$noSidebar && $sidebarPosition != 'left'}
            {include file='sidebar.tpl'}
        {/if}

	</div>
</section>

{include file='footer.tpl'}