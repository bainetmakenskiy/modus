{include file='header.tpl'}

<section class="wrapper">
            <div class="container"> 

                <section class="content {if $noSidebar}no-sidebar{/if} two-thirds">

<h2 class="page-header">{$aLang.password_reminder}</h2>
{$aLang.password_reminder_send_password}

				</section>

                {if !$noSidebar && $sidebarPosition != 'left'}
                    {include file='sidebar.tpl'}
                {/if}

            </div>
</section>

{include file='footer.tpl'}