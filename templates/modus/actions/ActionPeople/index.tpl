{include file='header.tpl' menu='people'}

<div class="people">
	<section class="wrapper white">
  		<div class="container people">

  		
  			<ul id="user-prefix-filter" class="search-abc">
				<li class="active"><a href="#" onclick="return ls.user.searchUsersByPrefix('',this);">{$aLang.user_search_filter_all}</a></li>
				{foreach from=$aPrefixUser item=sPrefixUser}
					<li><a href="#" onclick="return ls.user.searchUsersByPrefix('{$sPrefixUser}',this);">{$sPrefixUser}</a></li>
				{/foreach}
			</ul>

			<form action="" method="POST" id="form-users-search" onsubmit="return false;" class="search search-item">
				<input id="search-user-login" type="text" placeholder="{$aLang.user_search_title_hint}" autocomplete="off" name="user_login" value="" class="input-text" {if {cfg name='view.people.type'} == 'avatar'}style="width:815px;"{/if} onkeyup="ls.timer.run(ls.user.searchUsers,'users_search',['form-users-search'],1000);">
			</form>

		</div>
</section>
	<div id="users-list-search" style="display:none;"></div>
	<div id="users-list-original">
	{router page='people' assign=sUsersRootPage}
	{include file='user_list.tpl' aUsersList=$aUsersRating bUsersUseOrder=true sUsersRootPage=$sUsersRootPage}
	</div>
</div>

{include file='footer.tpl'}