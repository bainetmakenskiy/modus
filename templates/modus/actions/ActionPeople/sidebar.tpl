{hook run='people_sidebar_begin'}

			<section class="block">
              <header>
                  <h5>{$aLang.user_stats}</h5>
              </header>
              <ul class="arrow-circle-1">
                <li>{$aLang.user_stats_all}: <strong>{$aStat.count_all}</strong></li>
				<li>{$aLang.user_stats_active}: <strong>{$aStat.count_active}</strong></li>
				<li>{$aLang.user_stats_noactive}: <strong>{$aStat.count_inactive}</strong></li>
				<li>{$aLang.user_stats_sex_man}: <strong>{$aStat.count_sex_man}</strong></li>
				<li>{$aLang.user_stats_sex_woman}: <strong>{$aStat.count_sex_woman}</strong></li>
				<li>{$aLang.user_stats_sex_other}: <strong>{$aStat.count_sex_other}</strong></li>
              </ul>
          	</section>

{insert name="block" block='tagsCountry'}
{insert name="block" block='tagsCity'}

{hook run='people_sidebar_end'}