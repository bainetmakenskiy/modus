{assign var="sidebarPosition" value='right'}
{include file='header.tpl' menu="profile"}
<section class="wrapper">
            <div class="container"> 

                <section class="content {if $noSidebar}no-sidebar{/if} two-thirds">
				{include file='comment_list.tpl'}
 				</section>

                {if !$noSidebar && $sidebarPosition != 'left'}
                    {include file='sidebar.tpl'}
                {/if}

            </div>
</section>
{include file='footer.tpl'}