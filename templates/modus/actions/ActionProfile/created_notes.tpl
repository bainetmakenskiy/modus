{assign var="sidebarPosition" value='right'}
{include file='header.tpl' menu="profile"}

<section class="wrapper white">
            <div class="container talk"> 

                <section class="content {if $noSidebar}no-sidebar{/if} two-thirds">

{if $aNotes}
	<table class="table table-profile-notes" cellspacing="0">
		<thead class="{if {cfg name='view.table.th'} == 'theme1'}
						theme1
					  {elseif {cfg name='view.table.th'} == 'theme2'}
					  	theme2
					  {elseif {cfg name='view.table.th'} == 'theme3'}
					  	theme3
					  {/if}">
				<tr>
					<th class="cell-username">{$aLang.user}</th>
					<th class="cell-note">{$aLang.modus_profile_note_title_1}</th>
					<th class="cell-date">{$aLang.talk_inbox_date}</th>
				</tr>
		</thead>
		<tbody>
		{foreach from=$aNotes item=oNote}
			<tr>
				<td class="cell-username"><a href="{$oNote->getTargetUser()->getUserWebPath()}">{$oNote->getTargetUser()->getLogin()}</a></td>
				<td class="cell-note">{$oNote->getText()}</td>
				<td class="cell-date">{date_format date=$oNote->getDateAdd() format="j F Y"}</td>
			</tr>
		{/foreach}
		</tbody>
	</table>
{else}
	<div class="notice-empty">{$aLang.user_note_list_empty}</div>
{/if}


{include file='paging.tpl' aPaging=$aPaging}

				</section>

                {if !$noSidebar && $sidebarPosition != 'left'}
                    {include file='sidebar.tpl'}
                {/if}

            </div>
        </section>	

{include file='footer.tpl'}