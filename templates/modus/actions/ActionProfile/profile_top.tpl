<section class="wrapper">
    <div class="container profile">
    	{hook run='profile_top_begin' oUserProfile=$oUserProfile}
        <div class="foto width-3">
            <img src="{$oUserProfile->getProfileFotoPath()}" itemprop="photo" class="full-with js-infobox" id="foto-img" alt="{$oUserProfile->getLogin()}" title="{$oUserProfile->getLogin()}">

            {if $oUserCurrent and $oUserCurrent->getId() == $oUserProfile->getId()}
				<script type="text/javascript">
					jQuery(function($){
						$('#foto-upload').file({ name:'foto' }).choose(function(e, input) {
							ls.user.uploadFoto(null,input);
						});
					});
				</script>
		
				<p class="upload-photo border-radius">
					<a href="#" id="foto-upload" class="link-dotted">{if $oUserCurrent->getProfileFoto()}{$aLang.settings_profile_photo_change}{else}{$aLang.settings_profile_photo_upload}{/if}</a>&nbsp;&nbsp;&nbsp;
					<a href="#" id="foto-remove" class="link-dotted" onclick="return ls.user.removeFoto();" style="{if !$oUserCurrent->getProfileFoto()}display:none;{/if}">{$aLang.settings_profile_foto_delete}</a>
				</p>

				<div class="modal" id="foto-resize">
					<header class="modal-header">
						<h3>{$aLang.uploadimg}</h3>
					</header>
			
					<div class="modal-content">
						<img src="" alt="" id="foto-resize-original-img"><br />
						<button type="submit" class="button button-primary" onclick="return ls.user.resizeFoto();">{$aLang.settings_profile_avatar_resize_apply}</button>
						<button type="submit" class="button" onclick="return ls.user.cancelFoto();">{$aLang.settings_profile_avatar_resize_cancel}</button>
					</div>
				</div>
			{/if}

			{if !$oUserFriend}
			<div id="add_friend_form" class="modal">
				<header class="modal-header">
					<h3>{$aLang.profile_add_friend}</h3>
					<a href="#" class="close jqmClose"></a>
				</header>

				<form onsubmit="return ls.user.addFriend(this,{$oUserProfile->getId()},'add');" class="modal-content">
					<p><label for="add_friend_text">{$aLang.user_friend_add_text_label}</label>
					<textarea id="add_friend_text" rows="3" class="input-text input-width-full"></textarea></p>

					<button type="submit" class="button button-primary">{$aLang.user_friend_add_submit}</button>
				</form>
			</div>
			{/if}

            <div class="action border-radius">
                <ul>
                	{if $oUserCurrent && $oUserCurrent->getId()!=$oUserProfile->getId()}
                    	<li><a href="{router page='talk'}add/?talk_users={$oUserProfile->getLogin()}" title="{$aLang.user_write_prvmsg}" class="js-infobox"><i class="fa fa-envelope-o"></i></a></li>
                    	<li>
                    		<script type="text/javascript">
								jQuery(function($){
									ls.lang.load({lang_load name="profile_user_unfollow,profile_user_follow"});
								});
							</script>
                    		<a href="#" onclick="ls.user.followToggle(this, {$oUserProfile->getId()}); return false;" class="js-infobox" title="{if $oUserProfile->isFollow()}{$aLang.profile_user_unfollow}{else}{$aLang.profile_user_follow}{/if}"><i class="fa {if $oUserProfile->isFollow()}fa-eye-slash{else}fa-eye{/if}"></i></a></li>
                    		{include file='actions/ActionProfile/friend_item.tpl' oUserFriend=$oUserProfile->getUserFriend()}
                    {/if}
                    <li><span class="status">{if $oUserProfile->isOnline()}{$aLang.user_status_online}{else}{$aLang.user_status_offline}{/if}</span></li>
                </ul>
            </div>
        </div>
        
        <div class="topic-avatar width-9">
        {$aBlockParams.user=$oUserProfile}
		{insert name="block" block=modusProfileTopics params=$aBlockParams}
		</div>

        {hook run='profile_top_end' oUserProfile=$oUserProfile}
    </div>
</section>
