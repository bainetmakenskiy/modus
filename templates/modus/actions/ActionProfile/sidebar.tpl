		{hook run='profile_sidebar_begin' oUserProfile=$oUserProfile}

		{if $sAction=='talk' and $sEvent=='read'}
		{include file='actions/ActionTalk/speakers.tpl'}
		{/if}

		{if $sAction=='talk' and $sEvent=='add'}
		{include file='actions/ActionTalk/friends.tpl'}
		{/if}

		{if $sAction=='talk'}
		{include file='actions/ActionTalk/filter.tpl'}
		{/if}

		{if $sAction=='settings'}
		{include file='menu.settings.tpl'}
		{/if}

		{if $sAction=='profile'}
		{include file='menu.profile_created.tpl'}		

        {include file='menu.profile_favourite.tpl'}
        {/if}