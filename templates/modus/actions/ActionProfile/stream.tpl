{assign var="sidebarPosition" value='right'}
{include file='header.tpl' menu="profile"}

<section class="wrapper">
            <div class="container"> 

                <section class="content {if $noSidebar}no-sidebar{/if} two-thirds">
				{if count($aStreamEvents)}
					<ul class="stream-list" id="stream-list">
						{include file='actions/ActionStream/events.tpl'}
					</ul>

					{if !$bDisableGetMoreButton}
						<input type="hidden" id="stream_last_id" value="{$iStreamLastId}" />
						<a class="stream-get-more" id="stream_get_more" href="javascript:ls.stream.getMoreByUser({$oUserProfile->getId()})">{$aLang.stream_get_more} &darr;</a>
					{/if}
				{else}
					{$aLang.stream_no_events}
				{/if}
 				</section>

                {if !$noSidebar && $sidebarPosition != 'left'}
                    {include file='sidebar.tpl'}
                {/if}

            </div>
</section>

{include file='footer.tpl'}