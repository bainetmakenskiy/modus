{assign var="sidebarPosition" value='left'}
{assign var="sMenuItemSelect" value='profile'}
{include file='header.tpl' menu="profile"}

{assign var="oSession" value=$oUserProfile->getSession()}
{assign var="oVote" value=$oUserProfile->getVote()}
{assign var="oGeoTarget" value=$oUserProfile->getGeoTarget()}


			
{include file='actions/ActionProfile/profile_top.tpl'}

<section class="wrapper profile info-block">
    <div class="container user-about info border-radius">
        <div class="triangle-up"></div>
        <div class="text width-12">
          <h3>{$aLang.modus_profile_about_welcome}</h3>
          <p>{if $oUserProfile->getProfileName()}
          		{$aLang.modus_profile_about_welcome_name} {$oUserProfile->getProfileName()|escape:'html'}.
          	  {/if}

{assign var="aUserFieldValues" value=$oUserProfile->getUserFieldValues(true,array(''))}

{if $oUserProfile->getProfileSex()!='other' || $oUserProfile->getProfileBirthday() || $oGeoTarget || $oUserProfile->getProfileAbout() || count($aUserFieldValues)}
          	  {if $oGeoTarget}
          	  		{$aLang.profile_place}:
          	  		<span itemprop="address" itemscope itemtype="http://data-vocabulary.org/Address">
          	  		{if $oGeoTarget->getCountryId()}
          	  			<a href="{router page='people'}country/{$oGeoTarget->getCountryId()}/" itemprop="country-name">{$oUserProfile->getProfileCountry()|escape:'html'}</a>{if $oGeoTarget->getCityId()},{else}.{/if}
          	  		{/if}
          	  		{if $oGeoTarget->getCityId()}
						<a href="{router page='people'}city/{$oGeoTarget->getCityId()}/" itemprop="locality">{$oUserProfile->getProfileCity()|escape:'html'}</a>.
					{/if}
          	  		</span>
          	  {/if}
          	  {if $oUserProfile->getProfileBirthday()}
				{$aLang.profile_birthday}:</td>
				{date_format date=$oUserProfile->getProfileBirthday() format="j F Y"}.
			  {/if}
			  {if $oUserProfile->getProfileSex()!='other'}
					{$aLang.profile_sex}:
					{if $oUserProfile->getProfileSex()=='man'}
						{$aLang.profile_sex_man}.
					{else}
						{$aLang.profile_sex_woman}.
					{/if}
			  {/if}

			  {hook run='profile_whois_privat_item' oUserProfile=$oUserProfile}
{/if}
          	
          	  {if $oUserProfile->getProfileAbout()}					
					{$oUserProfile->getProfileAbout()}
			  {/if}
          	</p>
        </div>
    </div>
</section>


{if $oUserCurrent && $oUserCurrent->getId() != $oUserProfile->getId()}
<section class="wrapper profile info-block">
    <div class="container user-about info border-radius block-type-profile-note">
      <div class="triangle-up"></div>
      <section class="text width-12 border-radius">

          <h3>Заметка!</h3>

          {if $oUserNote}
              <script type="text/javascript">
                  ls.usernote.sText = {json var = $oUserNote->getText()};
              </script>
          {/if}

          <div id="usernote-note" class="profile-note" {if !$oUserNote}style="display: none;"{/if}>
              <p id="usernote-note-text">
                {if $oUserNote}
                  {$oUserNote->getText()}
                {/if}
              </p>
      
              <ul class="actions">
                  <li><a href="#" onclick="return ls.usernote.showForm();" class="js-infobox" title="{$aLang.user_note_form_edit}"><i class="fa fa-pencil"></i></a></li>
                  <li><a href="#" onclick="return ls.usernote.remove({$oUserProfile->getId()});" class="js-infobox" title="{$aLang.user_note_form_delete}"><i class="fa fa-remove"></i></a></li>
              </ul>
          </div>
    
          <div id="usernote-form" style="display: none;">
              <p><textarea rows="4" cols="20" id="usernote-form-text" class="input-text input-width-full"></textarea></p>
              <button type="submit" onclick="return ls.usernote.save({$oUserProfile->getId()});" class="button button-primary">{$aLang.user_note_form_save}</button>
              <button type="submit" onclick="return ls.usernote.hideForm();" class="button">{$aLang.user_note_form_cancel}</button>
          </div>

          <a href="#" onclick="return ls.usernote.showForm();" id="usernote-button-add" class="note-add js-infobox" {if $oUserNote}style="display:none;"{/if} title="{$aLang.user_note_add}"><i class="fa fa-pencil"></i></a>    
          
      </section>

    </div>
</section>
{/if}

<section class="wrapper">
    <div class="container profile about-more">
        <div class="title text_align_center">
          <h1>Информация</h1>
        </div>
        <div class="width-3">
            <h4>{$aLang.profile_activity}</h4>
            <ul class="arrow">
            	{if $oConfig->GetValue('general.reg.invite') and $oUserInviteFrom}
                	<li>{$aLang.profile_invite_from}: <a href="{$oUserInviteFrom->getUserWebPath()}">{$oUserInviteFrom->getLogin()}</a>&nbsp;</li>
                {/if}
                {if $oConfig->GetValue('general.reg.invite') and $aUsersInvite}
                	{foreach from=$aUsersInvite item=oUserInvite}
                		<li><a href="{$oUserInvite->getUserWebPath()}">{$oUserInvite->getLogin()}</a>&nbsp;</li>
                	{/foreach}
                {/if}

                {hook run='profile_whois_activity_item' oUserProfile=$oUserProfile}
	
				<li>{$aLang.profile_date_registration}: {date_format date=$oUserProfile->getDateRegister() day="day H:i" format="j F Y"}</li>
				{if $oSession}				
					<li>{$aLang.profile_date_last}: {date_format date=$oSession->getDateLast() day="day H:i" format="j F Y"}</li>
				{/if}

            </ul>


		{if $aBlogsOwner}
            <h4>{$aLang.profile_blogs_self}</h4>
            <ul class="arrow">
            	{foreach from=$aBlogsOwner item=oBlog name=blog_owner}
                	<li><a href="{$oBlog->getUrlFull()}">{$oBlog->getTitle()|escape:'html'}</a></li>
              	{/foreach}
            </ul>
        {/if}

	
		{if $aBlogAdministrators}
            <h4>{$aLang.profile_blogs_administration}</h4>
            <ul class="arrow">
            	{foreach from=$aBlogAdministrators item=oBlogUser name=blog_user}
					{assign var="oBlog" value=$oBlogUser->getBlog()}
                		<li><a href="{$oBlog->getUrlFull()}">{$oBlog->getTitle()|escape:'html'}</a></li>
                {/foreach}
            </ul>
        {/if}


		{if $aBlogModerators}
            <h4>{$aLang.profile_blogs_moderation}</h4>
            <ul class="arrow">
            	{foreach from=$aBlogModerators item=oBlogUser name=blog_user}
					{assign var="oBlog" value=$oBlogUser->getBlog()}
                	<li><a href="{$oBlog->getUrlFull()}">{$oBlog->getTitle()|escape:'html'}</a></li>
               	{/foreach}
            </ul>
        {/if}
	

		{if $aBlogUsers}
            <h4>{$aLang.profile_blogs_join}</h4>
            <ul class="arrow">
            	{foreach from=$aBlogUsers item=oBlogUser name=blog_user}
					{assign var="oBlog" value=$oBlogUser->getBlog()}
                	<li><a href="{$oBlog->getUrlFull()}">{$oBlog->getTitle()|escape:'html'}</a></li>
                {/foreach}
            </ul>
        {/if}

        </div>
        <div class="width-6">
            <div class="friends">
			{if $aUsersFriend}

              {include file='user_list_avatar.tpl' aUsersList=$aUsersFriend}

             {/if}

              {hook run='profile_whois_item_end' oUserProfile=$oUserProfile}
            </div>
            
            <div class="knob-bg width-2 vote minus">
              <a href="#" class="minus js-infobox" title="{$aLang.modus_profile_plus_title}" onclick="return ls.vote.vote({$oUserProfile->getId()},this,-1,'user');"><i class="fa fa-flip-horizontal fa-thumbs-o-down"></i></a>
              <div class="title">{$aLang.modus_menu_sup_minus}</div>
            </div>
            <div class="knob-bg width-2 js-infobox" title="{$aLang.user_vote_count}: {$oUserProfile->getCountVote()}">
              <input class="knob" data-fgColor="{if {cfg name='view.theme'} == 'green_orange'}#e36b2c{elseif {cfg name='view.theme'} == 'purpur_blue'}#20a8da{else}#e2534b{/if}" data-bgColor="{if {cfg name='view.theme'} == 'green_orange'}#70c570{elseif {cfg name='view.theme'} == 'purpur_blue'}#a65980{else}#76c7c0{/if}" data-width="120" data-angleOffset="90" data-thickness=".25" data-readOnly=true value="{$oUserProfile->getRating()}" id="vote_total_user_{$oUserProfile->getId()}">
              <div class="title">{$aLang.user_rating}</div>
            </div>
            <div class="knob-bg width-2 js-infobox" title="{$aLang.modus_profile_about_skill}">
              <input class="knob" data-fgColor="{if {cfg name='view.theme'} == 'green_orange'}#e36b2c{elseif {cfg name='view.theme'} == 'purpur_blue'}#20a8da{else}#e2534b{/if}" data-bgColor="{if {cfg name='view.theme'} == 'green_orange'}#70c570{elseif {cfg name='view.theme'} == 'purpur_blue'}#a65980{else}#76c7c0{/if}" data-width="120" data-angleOffset="90" data-thickness=".25" data-readOnly=true value="{$oUserProfile->getSkill()}" id="user_skill_{$oUserProfile->getId()}">
              <div class="title">{$aLang.user_skill}</div>
            </div>
            <div class="knob-bg width-2 vote plus">
              <a href="#" class="plus js-infobox" title="{$aLang.modus_profile_plus_title}" onclick="return ls.vote.vote({$oUserProfile->getId()},this,1,'user');"><i class="fa fa-thumbs-o-up"></i></a>
              <div class="title">{$aLang.modus_menu_sup_plus}</div>
            </div>

                    </div>
        <div class="width-3">
            <h4>{$aLang.profile_contacts}</h4>
            <ul class="arrow">
            	{assign var="aUserFieldValues" value=$oUserProfile->getUserFieldValues(true,array(''))}

				      {if $oUserProfile->getProfileSex()!='other' || $oUserProfile->getProfileBirthday() || $oGeoTarget || $oUserProfile->getProfileAbout() || count($aUserFieldValues)}

					         {if $aUserFieldValues}
						          {foreach from=$aUserFieldValues item=oField}
                			   <li>{$oField->getTitle()|escape:'html'}: {$oField->getValue(true,true)}</li>
                		  {/foreach}
					         {/if}
              {/if}

              {hook run='profile_whois_item_after_privat' oUserProfile=$oUserProfile}


              {assign var="aUserFieldContactValues" value=$oUserProfile->getUserFieldValues(true,array('contact'))}
					       {if $aUserFieldContactValues}
						        {foreach from=$aUserFieldContactValues item=oField}
							         <li>{$oField->getTitle()|escape:'html'}: {$oField->getValue(true,true)}</li>
						        {/foreach}
					       {/if}


				      {assign var="aUserFieldContactValues" value=$oUserProfile->getUserFieldValues(true,array('social'))}
					       {if $aUserFieldContactValues}
						        {foreach from=$aUserFieldContactValues item=oField}
							         <li>{$oField->getTitle()|escape:'html'}: {$oField->getValue(true,true)}</li>
						        {/foreach}
              {else}
                  {$aLang.modus_no_data_2}
					    {/if}

					    {hook run='profile_whois_item' oUserProfile=$oUserProfile}


            </ul>
        </div>

        {if {cfg name='view.profile.comments_list'} == 1}
          {$aBlockParams.user=$oUserProfile}
          {insert name="block" block=modusProfileComments params=$aBlockParams}
        {/if}
    </div>
</section>

{include file='footer.tpl'}