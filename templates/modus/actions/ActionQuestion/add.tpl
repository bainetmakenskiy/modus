{include file='header.tpl' menu='create'}

<section class="wrapper">
            <div class="container"> 

                <section class="content {if $noSidebar}no-sidebar{/if} two-thirds">

{include file='editor.tpl'}

{hook run='add_topic_question_begin'}


<form action="" method="POST" enctype="multipart/form-data" id="form-topic-add">
	{hook run='form_add_topic_question_begin'}
	
	<input type="hidden" name="security_ls_key" value="{$LIVESTREET_SECURITY_KEY}" />

	<p title="{$aLang.topic_create_blog_notice}" class="js-infobox"><label for="blog_id">{$aLang.topic_create_blog}</label>
	<select name="blog_id" id="blog_id" onChange="ls.blog.loadInfo(jQuery(this).val());" class="input-470">
		<option value="0">{$aLang.topic_create_blog_personal}</option>
		{foreach from=$aBlogsAllow item=oBlog}
			<option value="{$oBlog->getId()}" {if $_aRequest.blog_id==$oBlog->getId()}selected{/if}>{$oBlog->getTitle()|escape:'html'}</option>
		{/foreach}
	</select>
	</p>

	<script type="text/javascript">
		jQuery(document).ready(function($){
			ls.blog.loadInfo($('#blog_id').val());
		});
    </script>
	
	
	<p title="{$aLang.topic_question_create_title_notice}" class="js-infobox"><label for="topic_title">{$aLang.topic_question_create_title}:</label>
	<input type="text" id="topic_title" name="topic_title" value="{$_aRequest.topic_title}" class="input-text input-470" {if $bEditDisabled}readonly="readonly"{/if} /><br /></p>

	
	<div class="poll-create">
		<label>{$aLang.topic_question_create_answers}:</label>
		<ul class="question-list" id="question_list">
			{if count($_aRequest.answer)>=2}
				{foreach from=$_aRequest.answer item=sAnswer key=i}
					<li class="answer-add">
						<input type="text" value="{$sAnswer}" name="answer[]" class="input-text input-width-3" {if $bEditDisabled}disabled{/if} />
						{if !$bEditDisabled and $i>1} <a href="#" class="js-infobox" title="{$aLang.topic_question_create_answers_delete}" onClick="return ls.poll.removeAnswer(this);"></a>{/if}
					</li>
				{/foreach}
			{else}
				<li class="answer-add"><input type="text" value="" name="answer[]" class="input-text input-width-3" {if $bEditDisabled}disabled{/if} /></li>
				<li class="answer-add"><input type="text" value="" name="answer[]" class="input-text input-width-3" {if $bEditDisabled}disabled{/if} /></li>
			{/if}
		</ul>
	
		{if !$bEditDisabled}
			<a href="#" onClick="ls.poll.addAnswer(); return false;" class="button small">{$aLang.topic_question_create_answers_add}</a>
		{/if}

		<br><br><br>
	</div>

	
	<p><label for="topic_text">{$aLang.topic_question_create_text}:</label>
	<textarea name="topic_text" id="topic_text" rows="10" class="input-width-full mce-editor markitup-editor input-width-full">{$_aRequest.topic_text}</textarea>
	{if !$oConfig->GetValue('view.tinymce')}
		{include file='tags_help.tpl' sTagsTargetId="topic_text"}
	{/if}
	</p>

	
	<p class="js-infobox" title="{$aLang.topic_create_tags_notice}"><label for="topic_tags">{$aLang.topic_create_tags}:</label>
	<input type="text" id="topic_tags" name="topic_tags" value="{$_aRequest.topic_tags}" class="input-text input-470 autocomplete-tags-sep" />
	</p>

	
	<p title="{$aLang.topic_create_forbid_comment_notice}" class="js-infobox"><label for="topic_forbid_comment">
	<input type="checkbox" id="topic_forbid_comment" name="topic_forbid_comment" class="input-checkbox" value="1" {if $_aRequest.topic_forbid_comment==1}checked{/if} />
	{$aLang.topic_create_forbid_comment}</label>
	</p>

	
	{if $oUserCurrent->isAdministrator()}
		<p title="{$aLang.topic_create_publish_index_notice}" class="js-infobox"><label for="topic_publish_index">
		<input type="checkbox" id="topic_publish_index" name="topic_publish_index" class="input-checkbox" value="1" {if $_aRequest.topic_publish_index==1}checked{/if} />
		{$aLang.topic_create_publish_index}</label>
		</p>
	{/if}

	<input type="hidden" name="topic_type" value="question" />

	{hook run='form_add_topic_question_end'}

	<button type="submit" name="submit_topic_publish" id="submit_topic_publish" class="button button-primary fl-r">{$aLang.topic_create_submit_publish}</button>
	<button type="submit" name="submit_preview" onclick="jQuery('#text_preview').parent().show(); ls.topic.preview('form-topic-add','text_preview'); return false;" class="button">{$aLang.topic_create_submit_preview}</button>
	<button type="submit" name="submit_topic_save" id="submit_topic_save" class="button">{$aLang.topic_create_submit_save}</button>
</form>

<div class="topic-preview" id="text_preview"></div>

{hook run='add_topic_question_end'}

				</section>

                {if !$noSidebar && $sidebarPosition != 'left'}
                    {include file='sidebar.tpl'}
                {/if}

            </div>
</section>

{include file='footer.tpl'}