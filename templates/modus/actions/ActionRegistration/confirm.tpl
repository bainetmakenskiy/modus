{include file='header.tpl'}

<section class="wrapper">
            <div class="container"> 

                <section class="content {if $noSidebar}no-sidebar{/if} two-thirds">

<h2>{$aLang.registration_confirm_header}</h2>
{$aLang.registration_confirm_text}<br /><br />

<a href="{cfg name='path.root.web'}">{$aLang.site_go_main}</a>

				</section>

                {if !$noSidebar && $sidebarPosition != 'left'}
                    {include file='sidebar.tpl'}
                {/if}

            </div>
</section>

{include file='footer.tpl'}