{include file='header.tpl' menu='blog'}

<section class="wrapper">
            <div class="container"> 

                <section class="content {if $noSidebar}no-sidebar{/if} two-thirds">

{hook run='search_begin'}

<form action="{router page='search'}topics/" class="search">
	{hook run='search_form_begin'}
	<input type="text" placeholder="{$aLang.search}" maxlength="255" name="q" class="input-text input-width-full">
	<input type="submit" value="" title="{$aLang.search_submit}" class="input-submit">
	<i class="fa fa-search icon-search"></i>
	{hook run='search_form_end'}
</form>

{hook run='search_end'}

				</section>

                {if !$noSidebar && $sidebarPosition != 'left'}
                    {include file='sidebar.tpl'}
                {/if}

            </div>
</section>

{include file='footer.tpl'}