{assign var="sidebarPosition" value='right'}
{include file='header.tpl' menu="talk"}

<section class="wrapper white">
            <div class="container talk"> 

                <section class="content {if $noSidebar}no-sidebar{/if} two-thirds">

<section class="block">
	<header class="block-header">
		<h3>{$aLang.talk_blacklist_title}</h3>
	</header>


	<div class="block-content">
		<form onsubmit="return ls.talk.addToBlackList();">
			<p class="js-infobox" title="{$aLang.talk_balcklist_add_label}"><label for="talk_blacklist_add"></label>
			<input type="text" id="talk_blacklist_add" name="add" class="input-text input-width-8 autocomplete-users-sep" /></p>
		</form>


		<div id="black_list_block">
			{if $aUsersBlacklist}
				<ul class="list" id="black_list">
					{foreach from=$aUsersBlacklist item=oUser}
						<li id="blacklist_item_{$oUser->getId()}_area"><a href="{$oUser->getUserWebPath()}" class="user">{$oUser->getLogin()}</a> - <a href="#" id="blacklist_item_{$oUser->getId()}" class="delete">{$aLang.blog_delete}</a></li>
					{/foreach}
				</ul>
			{/if}
		</div>
	</div>
</section>

                </section>

                {if !$noSidebar && $sidebarPosition != 'left'}
                    {include file='sidebar.tpl'}
                {/if}

            </div>
        </section>

{include file='footer.tpl'}