{if $oTopic}
	{assign var="oBlog" value=$oTopic->getBlog()}
	{if $oBlog->getType()!='personal'}
		<section class="block">
              <header>
                  <a href="{$oBlog->getUrlFull()}" class="title js-infobox" title="{$aLang.modus_sidebar_blog_info_title}"><h5>{$oBlog->getTitle()|escape:'html'}</h5></a>
              </header>

              <ul class="arrow">
                  <li id="blog_user_count_{$oBlog->getId()}">{$oBlog->getCountUser()} {$oBlog->getCountUser()|declension:$aLang.reader_declension:'russian'}</li>
                  <li>{$oBlog->getCountTopic()} {$oBlog->getCountTopic()|declension:$aLang.topic_declension:'russian'}</li>
              </ul>			

              {if $oUserCurrent and $oUserCurrent->getId()!=$oBlog->getOwnerId()}
					<button type="submit" class="button button-primary {if $oBlog->getUserIsJoin()}active{/if}" id="blog-join" data-only-text="1" onclick="ls.blog.toggleJoin(this,{$oBlog->getId()}); return false;">{if $oBlog->getUserIsJoin()}{$aLang.blog_leave}{else}{$aLang.blog_join}{/if}</button>&nbsp;&nbsp;
			  {/if}

			  <footer>
					<a href="{router page='rss'}blog/{$oBlog->getUrl()}/" class="rss">RSS</a>
			  </footer>
        </section>
	{/if}
{/if}