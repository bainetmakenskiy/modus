		<section class="block blogs">
              <header>
                  <h5>{$aLang.block_blog_info}</h5>
              </header>

              <div class="block-content">
					<p id="block_blog_info"></p>
			  </div>

        </section>
        <section class="block blogs">
              <header>
                  <h5>{$aLang.block_blog_info_note}</h5>
              </header>

              <div class="block-content">
					<p>{$aLang.block_blog_info_note_text}</p>
			  </div>

        </section>