		<section class="block blogs" id="block_blogs">
              <header>
                  <h5>{$aLang.block_blogs}</h5>
                  <div class="block-update js-block-blogs-update"></div>
              </header>

              <div class="js-block-blogs-content">
					{$sBlogsTop}
			  </div>

			  <footer>					
					<ul class="nav-block js-block-blogs-nav">
						<li><a href="{router page='blogs'}">{$aLang.block_blogs_all}</a></li>
						<li class="active js-block-blogs-item" data-type="top"><a href="#">{$aLang.block_blogs_top}</a></li>
						{if $oUserCurrent}
							<li class="js-block-blogs-item" data-type="join"><a href="#">{$aLang.block_blogs_join}</a></li>
							<li class="js-block-blogs-item" data-type="self"><a href="#">{$aLang.block_blogs_self}</a></li>
						{/if}
					</ul>
			  </footer>
        </section>


