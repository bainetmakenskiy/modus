		{if {cfg name='view.sidebar.blogs'} == 'list'}
			<ul class="arrow-circle">
				{foreach from=$aBlogs item=oBlog}
                  <li class="js-infobox" title="{$aLang.blog_rating}: {$oBlog->getRating()}"><a href="{$oBlog->getUrlFull()}">{$oBlog->getTitle()|escape:'html'}</a></li>
                {/foreach}
            </ul>
        {elseif {cfg name='view.sidebar.blogs'} == 'avatar'}
            <ul class="avatar clearfix">
				{foreach from=$aBlogs item=oBlog}
                  <li class="js-infobox" title="{$oBlog->getTitle()|escape:'html'}">
                  	<a href="{$oBlog->getUrlFull()}">
                  		<img src="{$oBlog->getAvatarPath(70)}" class="img-rounded" alt="{$oBlog->getTitle()|escape:'html'}" />
                  	</a>
                  </li>
                {/foreach}
            </ul>
        {/if}			