{if $modustpl_aCommentsProfile and count($modustpl_aCommentsProfile)}
<div class="user-profile-comments">
<div class="title text_align_center">
          <a href="{$oUserProfile->getUserWebPath()}created/comments/"><h2>{$aLang.modus_profile_comments}</h2></a>
        </div>
	{include file='comment_list.tpl' aComments=$modustpl_aCommentsProfile}

</div>
{/if}