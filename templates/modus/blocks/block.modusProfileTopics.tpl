{if $modustpl_aTopicsProfile and count($modustpl_aTopicsProfile)}

	<div class="row-1">
		{foreach from=$modustpl_aTopicsProfile item=oTopic}
			{assign var="oBlog" value=$oTopic->getBlog()}
				{assign var="oUser" value=$oTopic->getUser()}
				{assign var="oVote" value=$oTopic->getVote()}
       
                <div class="width-2">
                  <a href="{$oTopic->getUrl()}">                 
                  	{if $oTopic->getType()=='photoset' and $oTopic->getPreviewImage()}
                    	<img class="full-with js-infobox" src="{$oTopic->getPreviewImageWebPath('200crop')}" alt="{$oTopic->getTitle()}" title="{$oTopic->getTitle()}" />
                    {elseif $oTopic->getPreviewImage()}
                    	<img class="full-with js-infobox" alt="{$oTopic->getTitle()}" title="{$oTopic->getTitle()}" src="{$oTopic->getPreviewImageWebPath('200crop')}"/>
                    {else}
                  		<img class="full-with js-infobox" alt="{$oTopic->getTitle()}" title="{$oTopic->getTitle()}" src="{cfg name="path.static.skin"}/images/topic-avatar-420x420.png" />
                    {/if}
                  </a>
                </div>
            
        {/foreach}
    </div>

            <div class="row-2"> 
                  <a href="{$oUserProfile->getUserWebPath()}created/topics/" class="button js-infobox" title="{$aLang.modus_profile_top_go_topic}"><i class="fa fa-angle-double-right"></i></a>      
            </div>
{/if}

<!-- img 420x420 -->