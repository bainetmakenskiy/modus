{if $modustpl_aTopicsTop and count($modustpl_aTopicsTop)}
      <section class="wrapper white">
        <section class="container">
      <div class="slider-wrapper {if {cfg name='view.topic.slider_theme'} == 'new'}
                                                  theme-new
                                                {elseif {cfg name='view.topic.slider_theme'} == 'bar'}
                                                  theme-bar
                                                {elseif {cfg name='view.topic.slider_theme'} == 'default'}
                                                  theme-default
                                                {elseif {cfg name='view.topic.slider_theme'} == 'light'}
                                                  theme-light
                                                {elseif {cfg name='view.topic.slider_theme'} == 'dark'}
                                                  theme-dark
                                                {/if} container">
            <div id="slider" class="nivoSlider">
            {foreach from=$modustpl_aTopicsTop item=oTopic}
                  {assign var="oUser" value=$oTopic->getUser()}
                    {assign var=oMainPhoto value=$oTopic->getPhotosetMainPhoto()}               
                        {if $oMainPhoto}      
                              <img src="{$oMainPhoto->getWebPath(1200crop)}" data-thumb="{$oMainPhoto->getWebPath(1200crop)}" data-transition="slideInLeft" alt="{$oTopic->getTitle()|escape:'html'}" title="#{$oTopic->getId()}" id="photoset-main-image-{$oTopic->getId()}" />
                        {else}
                              {if $oTopic->getType()=='photoset' and $oTopic->getPreviewImage()}
                                    <img src="{$oTopic->getPreviewImageWebPath('1200crop')}" data-thumb="{$oTopic->getPreviewImageWebPath('1200crop')}" data-transition="slideInLeft" alt="{$oTopic->getTitle()|escape:'html'}" title="#{$oTopic->getId()}" />
                              {elseif $oTopic->getPreviewImage()}
                                    <img src="{$oTopic->getPreviewImageWebPath('1200crop')}" data-thumb="{$oTopic->getPreviewImageWebPath('1200crop')}" data-transition="slideInLeft" alt="{$oTopic->getTitle()|escape:'html'}" title="#{$oTopic->getId()}" />
                              {else}
                                    <img src="{cfg name='path.static.skin'}/images/slide.png" data-thumb="{cfg name='path.static.skin'}/images/slide.png" data-transition="slideInLeft" alt="" title="#{$oTopic->getId()}" />
                              {/if}
                        {/if}
            {/foreach}
            </div>

            {foreach from=$modustpl_aTopicsTop item=oTopic}
                  {assign var="oUser" value=$oTopic->getUser()}
                  <div id="{$oTopic->getId()}" class="nivo-html-caption">
                        <div class="align_center text-center text_align_center">
                              <a href="{$oTopic->getUrl()}"><h2 class="border-radius">{$oTopic->getTitle()|escape:'html'|strip_tags|truncate:15:''}</h2></a>
                              <p class="border-radius">{$oTopic->getTextShort()|strip_tags|truncate:100:'...'}</p>
                        </div>
                  </div>
            {/foreach}
        </section>
      </section>
{/if}