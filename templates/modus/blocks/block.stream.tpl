		<section class="block stream">
              <header>
                  <a href="#" class="title js-block-stream-item" data-type="comment"><h5 class="js-infobox" title="{$aLang.modus_sidebar_update}">{$aLang.block_stream}</h5></a>
              </header>

              {hook run='block_stream_nav_item' assign="sItemsHook"}

		
              {$sItemsHook}

              <div class="js-block-stream-content">
					{$sStreamComments}
			  </div>

			  <footer>
			  		<a href="{router page='comments'}">{$aLang.block_stream_comments_all}</a> | <a href="{router page='rss'}allcomments/">RSS</a>
			  </footer>
		</section>




