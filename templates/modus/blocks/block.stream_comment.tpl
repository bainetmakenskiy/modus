			<ul class="arrow-1">
				{foreach from=$aComments item=oComment name="cmt"}
					{assign var="oUser" value=$oComment->getUser()}
					{assign var="oTopic" value=$oComment->getTarget()}
					{assign var="oBlog" value=$oTopic->getBlog()}
                  	<li class="js-title-comment" title="{if {cfg name='view.login.name'} == '1'}
                          {if $oUser->getProfileName()}
                                {$oUser->getProfileName()|escape:'html'}
                          {else}
                                {$oUser->getLogin()}
                          {/if}
                      {else}
                                {$oUser->getLogin()}
                      {/if}: {$oComment->getText()|strip_tags|trim|truncate:100:'...'|escape:'html'}">
                    	<a href="{if $oConfig->GetValue('module.comment.nested_per_page')}{router page='comments'}{else}{$oTopic->getUrl()}#comment{/if}{$oComment->getId()}">{$oTopic->getTitle()|escape:'html'}</a>
                    	<time datetime="{date_format date=$oComment->getDate() format='c'}">{date_format date=$oComment->getDate() hours_back="12" minutes_back="60" now="60" day="day H:i" format="j F Y, H:i"}</time>
                  	</li>
              	{/foreach}
            </ul>