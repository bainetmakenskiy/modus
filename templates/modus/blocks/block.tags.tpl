<section class="block tags">
  <header class="block-header">
    <h5>{$aLang.block_tags}</h5>
  </header>
  
  
  <div class="block-content clearfix">

    <div class="js-block-tags-content" data-type="all">
      {if $aTags}
        <ul class="tag-cloud word-wrap">
          {foreach from=$aTags item=oTag}
            <li><a class="tag-size-{$oTag->getSize()}" href="{router page='tag'}{$oTag->getText()|escape:'url'}/">{$oTag->getText()|escape:'html'}</a></li>
          {/foreach}
        </ul>
      {else}
        <div class="notice-empty">{$aLang.block_tags_empty}</div>
      {/if}
    </div>

    {if $oUserCurrent}
      <div class="js-block-tags-content" data-type="user" style="display: none;">
        {if $aTagsUser}
          <ul class="tag-cloud word-wrap">
            {foreach from=$aTagsUser item=oTag}
              <li><a class="tag-size-{$oTag->getSize()}" href="{router page='tag'}{$oTag->getText()|escape:'url'}/">{$oTag->getText()|escape:'html'}</a></li>
            {/foreach}
          </ul>
          {else}
          <div class="notice-empty">{$aLang.block_tags_empty}</div>
        {/if}
      </div>
    {/if}

  </div>
  <footer>
     
          <a href="#" class="active js-block-tags-item" data-type="all">{$aLang.topic_favourite_tags_block_all}</a>
      {if $oUserCurrent}
          | <a href="#" class="js-block-tags-item" data-type="user">{$aLang.topic_favourite_tags_block_user}</a>
      {/if}

      {hook run='block_tags_nav_item'}
      </ul>
  </footer>
</section>