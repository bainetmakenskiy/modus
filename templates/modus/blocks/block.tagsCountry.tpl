{if $aCountryList && count($aCountryList)>0}
	<section class="block tags">
		<header class="block-header">
			<h5>{$aLang.block_country_tags}</h5>
		</header>
		
		
		<div class="block-content">
			<ul class="tag-cloud word-wrap">
				{foreach from=$aCountryList item=oCountry}
					<li><a class="tag-size-{$oCountry->getSize()}" href="{router page='people'}country/{$oCountry->getId()}/">{$oCountry->getName()|escape:'html'}</a></li>
				{/foreach}					
			</ul>	
		</div>		
	</section>
{/if}