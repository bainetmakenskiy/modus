		<section class="block blogs" id="block_blogs">
              <header>
                  <a href="#" onclick="jQuery('#block_favourite_topic_content').toggle(); return false;"><h5>{$aLang.topic_favourite_tags_block}</h5></a>
              </header>

    		  <div class="block-content" id="block_favourite_topic_content">
					
					<div class="js-block-favourite-topic-tags-content" data-type="all">
						{if $aFavouriteTopicTags}
							<ul class="tag-cloud word-wrap arrow-circle-1">
								{foreach from=$aFavouriteTopicTags item=oTag}
									<li><a class="tag-size-{$oTag->getSize()} {if $sFavouriteTag==$oTag->getText()}tag-current{/if}" title="{$oTag->getCount()}" href="{$oFavouriteUser->getUserWebPath()}favourites/topics/tag/{$oTag->getText()|escape:'url'}/">{$oTag->getText()}</a></li>
								{/foreach}
							</ul>
						{else}
							<div class="notice-empty">{$aLang.block_tags_empty}</div>
						{/if}
					</div>
		
					<div class="js-block-favourite-topic-tags-content" data-type="user" style="display: none;">
						{if $aFavouriteTopicUserTags}
							<ul class="tag-cloud word-wrap">
								{foreach from=$aFavouriteTopicUserTags item=oTag}
									<li><a class="tag-size-{$oTag->getSize()}" title="{$oTag->getCount()}" href="{$oFavouriteUser->getUserWebPath()}favourites/topics/tag/{$oTag->getText()|escape:'url'}/">{$oTag->getText()}</a></li>
								{/foreach}
							</ul>
						{else}
							<div class="notice-empty">{$aLang.block_tags_empty}</div>
						{/if}
					</div>

					<ul class="nav nav-pills">
						<li></li>
						</li>

						{hook run='block_favourite_topic_tags_nav_item'}
					</ul>
			  </div>

			  <footer>
			  		<a href="#" class="active js-block-favourite-topic-tags-item" data-type="all">{$aLang.topic_favourite_tags_block_all}</a>
			  		|
			  		<a href="#" class="js-block-favourite-topic-tags-item" data-type="user">{$aLang.topic_favourite_tags_block_user}</a>
			  </footer>
              
        </section>