{if $oUserCurrent}

		<section class="block stream-settings-blogs js-infobox" title="{$aLang.userfeed_settings_note_follow_blogs}">
              <header>
                  <h5>{$aLang.userfeed_block_blogs_title}</h5>
              </header>

              <div class="block-content">

				{if count($aUserfeedBlogs)}
					<ul class="stream-settings-blogs">
						{foreach from=$aUserfeedBlogs item=oBlog}
							{assign var=iBlogId value=$oBlog->getId()}
							<li><input class="userfeedBlogCheckbox padding"
									type="checkbox"
									{if isset($aUserfeedSubscribedBlogs.$iBlogId)} checked="checked"{/if}
									onClick="if (jQuery(this).prop('checked')) { ls.userfeed.subscribe('blogs',{$iBlogId}) } else { ls.userfeed.unsubscribe('blogs',{$iBlogId}) } " />
								<a href="{$oBlog->getUrlFull()}">{$oBlog->getTitle()|escape:'html'}</a>
							</li>
						{/foreach}
					</ul>
				{else}
					<small class="notice-empty">{$aLang.userfeed_no_blogs}</small>
				{/if}
			  </div>

			 
        </section>
{/if}