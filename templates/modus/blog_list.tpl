{if {cfg name='view.blogs.type'} == 'avatar'}

	{if $aBlogs}
		{foreach from=$aBlogs item=oBlog}
				{assign var="oUserOwner" value=$oBlog->getOwner()}
					<section class="topic topic-skin-theme4 view {if {cfg name='view.blogs.theme_style'} == 'default'}
	view-default
	{elseif {cfg name='view.blogs.theme_style'} == 'first'}
	view-first
	{elseif {cfg name='view.blogs.theme_style'} == 'second'}
	view-second
	{elseif {cfg name='view.blogs.theme_style'} == 'third'}
	view-third
	{elseif {cfg name='view.blogs.theme_style'} == 'fourth'}
	view-fourth
	{elseif {cfg name='view.blogs.theme_style'} == 'fifth'}
	view-fifth
	{elseif {cfg name='view.blogs.theme_style'} == 'sixth'}
	view-sixth
	{elseif {cfg name='view.blogs.theme_style'} == 'seventh'}
	view-seventh
	{elseif {cfg name='view.blogs.theme_style'} == 'eighth'}
	view-eighth
	{elseif {cfg name='view.blogs.theme_style'} == 'tenth'}
	view-tenth
	{/if}">
          				<div class="topic-avatar">
            				<img src="{$oBlog->getAvatarPath(260)}" class="full-with img-rounded" alt="{$oBlog->getTitle()|escape:'html'}" />
          				</div>
          				<div class="mask">
              				<p>{$oBlog->getDescription()|strip_tags|truncate:130:'...'}</p>
         
              				<div class="info">
              					{if $oUserCurrent and $oUserCurrent->getId()!=$oBlog->getOwnerId()}
              						<a href="#" class="border-radius js-infobox" onclick="ls.blog.toggleJoin(this,{$oBlog->getId()}); return false;" title="{if $oBlog->getUserIsJoin()}{$aLang.blog_leave}{else}{$aLang.blog_join}{/if}"><i class="fa {if $oBlog->getUserIsJoin()}fa-toggle-on{else}fa-toggle-off{/if}"></i></a>
              					{/if}
                				<a href="{$oBlog->getUrlFull()}" class="border-radius"><i class="fa fa-link"></i></a>
              				</div>   
              
              				<h2>
              				{if $oBlog->getType() == 'close'}
								<i class="fa fa-key js-infobox" title="{$aLang.blog_closed}"></i>
							{/if}
							{$oBlog->getTitle()|escape:'html'}
							</h2>  

          				</div>
					</section>
		{/foreach}
	{else}
		<p>
		{if $sBlogsEmptyList}
			{$sBlogsEmptyList}
		{else}

		{/if}
		</p>
	{/if}

{elseif {cfg name='view.blogs.type'} == 'table'}

<table class="table table-blogs">
	{if $bBlogsUseOrder}
		<thead class="{if {cfg name='view.table.th'} == 'theme1'}
						theme1
					  {elseif {cfg name='view.table.th'} == 'theme2'}
					  	theme2
					  {elseif {cfg name='view.table.th'} == 'theme3'}
					  	theme3
					  {/if}">
			<tr>
				<th class="cell-name"><a href="{$sBlogsRootPage}?order=blog_title&order_way={if $sBlogOrder=='blog_title'}{$sBlogOrderWayNext}{else}{$sBlogOrderWay}{/if}" {if $sBlogOrder=='blog_title'}class="{$sBlogOrderWay}"{/if}>{$aLang.blogs_title}</a></th>

				{if $oUserCurrent}
					<th class="cell-join">{$aLang.blog_join_leave}</th>
				{/if}

				<th class="cell-readers">
					<a href="{$sBlogsRootPage}?order=blog_count_user&order_way={if $sBlogOrder=='blog_count_user'}{$sBlogOrderWayNext}{else}{$sBlogOrderWay}{/if}" {if $sBlogOrder=='blog_count_user'}class="{$sBlogOrderWay}"{/if}>{$aLang.blogs_readers}</a>
				</th>
				<th class="cell-rating align-center"><a href="{$sBlogsRootPage}?order=blog_rating&order_way={if $sBlogOrder=='blog_rating'}{$sBlogOrderWayNext}{else}{$sBlogOrderWay}{/if}" {if $sBlogOrder=='blog_rating'}class="{$sBlogOrderWay}"{/if}>{$aLang.blogs_rating}</a></th>
			</tr>
		</thead>
	{else}
		<thead class="{if {cfg name='view.table.th'} == 'theme1'}
						theme1
					  {elseif {cfg name='view.table.th'} == 'theme2'}
					  	theme2
					  {elseif {cfg name='view.table.th'} == 'theme3'}
					  	theme3
					  {/if}">
			<tr>
				<th class="cell-name">{$aLang.blogs_title}</th>

				{if $oUserCurrent}
					<th class="cell-join">{$aLang.blog_join_leave}</th>
				{/if}

				<th class="cell-readers">{$aLang.blogs_readers}</th>
				<th class="cell-rating align-center">{$aLang.blogs_rating}</th>
			</tr>
		</thead>
	{/if}
	
	
	<tbody>
		{if $aBlogs}
			{foreach from=$aBlogs item=oBlog}
				{assign var="oUserOwner" value=$oBlog->getOwner()}

				<tr>
					<td class="cell-name">
						<a href="{$oBlog->getUrlFull()}">
							<img src="{$oBlog->getAvatarPath(48)}" width="48" height="48" alt="avatar" class="avatar" />
						</a>
						
						<p>
							<a href="#" onclick="return ls.infobox.showInfoBlog(this,{$oBlog->getId()});" class="icon-question-sign"></a>

							{if $oBlog->getType() == 'close'}
								<i class="fa fa-key js-infobox" title="{$aLang.blog_closed}"></i>
							{/if}

							<a href="{$oBlog->getUrlFull()}">{$oBlog->getTitle()|escape:'html'}</a>
						</p>
					</td>

					{if $oUserCurrent}
						<td class="cell-join">
							{if $oUserCurrent->getId() != $oBlog->getOwnerId() and $oBlog->getType() == 'open'}
								<a href="#" onclick="ls.blog.toggleJoin(this, {$oBlog->getId()}); return false;" class="link-dotted">
									{if $oBlog->getUserIsJoin()}
										{$aLang.blog_leave}
									{else}
										{$aLang.blog_join}
									{/if}
								</a>
							{else}
								&mdash;
							{/if}
						</td>
					{/if}

					<td class="cell-readers" id="blog_user_count_{$oBlog->getId()}">{$oBlog->getCountUser()}</td>
					<td class="cell-rating align-center">{$oBlog->getRating()}</td>
				</tr>
			{/foreach}
		{else}
			<tr>
				<td colspan="3">
					{if $sBlogsEmptyList}
						{$sBlogsEmptyList}
					{else}

					{/if}
				</td>
			</tr>
		{/if}
	</tbody>
</table>
{/if}