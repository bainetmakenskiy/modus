{assign var="oUser" value=$oComment->getUser()}
{assign var="oVote" value=$oComment->getVote()}


<section id="comment_id_{$oComment->getId()}" class="comment">
	{if !$oComment->getDelete() or $bOneComment or ($oUserCurrent and $oUserCurrent->isAdministrator())}
		<a name="comment{$oComment->getId()}"></a>
		
		
		<a href="{$oUser->getUserWebPath()}" class="js-infobox" title="{$oUser->getLogin()}"><img src="{$oUser->getProfileAvatarPath(100)}" width="70" height="70" alt="avatar" class="comment-avatar img-rounded" /></a>
		
		
		<ul class="comment-info">
			<li class="comment-author">
				<a href="{$oUser->getUserWebPath()}" class="js-infobox" {if $iAuthorId == $oUser->getId()}title="{if $sAuthorNotice}{$sAuthorNotice}{/if}"{/if}>
					{if {cfg name='view.login.name'} == '1'}
                          {if $oUser->getProfileName()}
                                {$oUser->getProfileName()|escape:'html'}
                          {else}
                                {$oUser->getLogin()}
                          {/if}
                    {else}
                                {$oUser->getLogin()}
                    {/if}
				</a>
			</li>
			<li class="comment-date">
				<a href="{if $oConfig->GetValue('module.comment.nested_per_page')}{router page='comments'}{else}#comment{/if}{$oComment->getId()}" class="js-infobox" title="{$aLang.comment_url_notice}">
					<time datetime="{date_format date=$oComment->getDate() format='c'}">{date_format date=$oComment->getDate() hours_back="12" minutes_back="60" now="60" day="day H:i" format="j F Y, H:i"}</time>
				</a>
			</li>
			
			{if $oComment->getPid()}
				<li class="goto-comment-parent"><a href="#" onclick="ls.comments.goToParentComment({$oComment->getId()},{$oComment->getPid()}); return false;" title="{$aLang.comment_goto_parent}"><i class="fa fa-chevron-up"></i></a></li>
			{/if}
			<li class="goto-comment-child"><a href="#" title="{$aLang.comment_goto_child}"><i class="fa fa-chevron-down"></i></a></li>

			{if $oUserCurrent}
				{if !$oComment->getDelete() and !$bAllowNewComment}
					<li><a href="#"  class="reply-link link-dotted"></a></li>
					<li class="reply comment"><a href="#" onclick="ls.comments.toggleCommentForm({$oComment->getId()}); return false;"><i class="fa fa-rotate-left"></i> {$aLang.comment_answer}</a></li>
				{/if}
					
				{if !$oComment->getDelete() and $oUserCurrent and $oUserCurrent->isAdministrator()}
					<li class="reply comment-delete"><a href="#" onclick="ls.comments.toggle(this,{$oComment->getId()}); return false;"><i class="fa fa-remove"></i> {$aLang.comment_delete}</a></li>
				{/if}
				
				{if $oComment->getDelete() and $oUserCurrent and $oUserCurrent->isAdministrator()}
					<li class="reply comment-repair"><a href="#" onclick="ls.comments.toggle(this,{$oComment->getId()}); return false;"><i class="fa fa-reply"></i> {$aLang.comment_repair}</a></li>
				{/if}

				{if $oUserCurrent and !$bNoCommentFavourites}
					<li class="reply comment-favourite">
						<a href="#" onclick="return ls.favourite.toggle({$oComment->getId()},this,'comment');"><i class="fa fa-star"></i> {$aLang.modus_add_favorite} <span class="favourite-count" id="fav_count_comment_{$oComment->getId()}">{if $oComment->getCountFavourite() > 0}({$oComment->getCountFavourite()}){/if}</span></a>
					</li>
				{/if}
				
				{hook run='comment_action' comment=$oComment}
			{/if}
			
			
			{if $oComment->getTargetType() != 'talk'}						
				<li id="vote_area_comment_{$oComment->getId()}" class="vote 
																		{if $oComment->getRating() > 0}
																			vote-count-positive
																		{elseif $oComment->getRating() < 0}
																			vote-count-negative
																		{/if}    
																		
																		{if $oVote} 
																			voted 
																			
																			{if $oVote->getDirection() > 0}
																				voted-up
																			{else}
																				voted-down
																			{/if}
																		{/if}">
					<div class="vote-down" onclick="return ls.vote.vote({$oComment->getId()},this,-1,'comment');"><i class="fa fa-thumbs-o-down"></i></div>
					<span class="vote-count" id="vote_total_comment_{$oComment->getId()}">{if $oComment->getRating() > 0}+{/if}{$oComment->getRating()}</span>
					<div class="vote-up" onclick="return ls.vote.vote({$oComment->getId()},this,1,'comment');"><i class="fa fa-thumbs-o-up"></i></div>
				</li>
			{/if}
			
		
		</ul>
		
		
		<div id="comment_content_id_{$oComment->getId()}" class="border-radius comment-content {if $oComment->isBad()}
															comment-bad
														{/if}

														{if $oComment->getDelete()}
															comment-deleted
														{elseif $oUserCurrent and $oComment->getUserId() == $oUserCurrent->getId()} 
															comment-self
														{elseif $sDateReadLast <= $oComment->getDate()} 
															comment-new
														{/if} text">
			{$oComment->getText()}
		</div>
			
	{else}				
		{$aLang.comment_was_delete}
	{/if}	
</section>