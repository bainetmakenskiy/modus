<div class="comments comment-list">
	{foreach from=$aComments item=oComment}
		{assign var="oUser" value=$oComment->getUser()}
		{assign var="oTopic" value=$oComment->getTarget()}
		{assign var="oBlog" value=$oTopic->getBlog()}
		
		
		{assign var="oUser" value=$oComment->getUser()}
{assign var="oVote" value=$oComment->getVote()}


<section id="comment_id_{$oComment->getId()}" class="comment">
	{if !$oComment->getDelete() or $bOneComment or ($oUserCurrent and $oUserCurrent->isAdministrator())}
		<a name="comment{$oComment->getId()}"></a>
		
		
		<a href="{$oUser->getUserWebPath()}" class="js-infobox" title="{$oUser->getLogin()}"><img src="{$oUser->getProfileAvatarPath(100)}" width="70" height="70" alt="avatar" class="comment-avatar img-rounded" /></a>
		
		
		<ul class="comment-info">
			<li class="comment-author">
				<a href="{$oUser->getUserWebPath()}" class="js-infobox" {if $iAuthorId == $oUser->getId()}title="{if $sAuthorNotice}{$sAuthorNotice}{/if}"{/if}>{$oUser->getLogin()}</a>
			</li>
			<li class="comment-date">
				<a href="{$oTopic->getUrl()}#comments" class="js-infobox" title="{$aLang.comment_url_notice}">
					<time datetime="{date_format date=$oComment->getDate() format='c'}">{date_format date=$oComment->getDate() hours_back="12" minutes_back="60" now="60" day="day H:i" format="j F Y, H:i"}</time>
				</a>
			</li>
			
			{if $oComment->getPid()}
				<li class="goto-comment-parent"><a href="#" onclick="ls.comments.goToParentComment({$oComment->getId()},{$oComment->getPid()}); return false;" title="{$aLang.comment_goto_parent}"><i class="fa fa-chevron-up"></i></a></li>
			{/if}

			{if $oUserCurrent}
					
				{if !$oComment->getDelete() and $oUserCurrent and $oUserCurrent->isAdministrator()}
					<li class="reply comment-delete"><a href="#" onclick="ls.comments.toggle(this,{$oComment->getId()}); return false;"><i class="fa fa-remove"></i> {$aLang.comment_delete}</a></li>
				{/if}
				
				{if $oComment->getDelete() and $oUserCurrent and $oUserCurrent->isAdministrator()}
					<li class="reply comment-repair"><a href="#" onclick="ls.comments.toggle(this,{$oComment->getId()}); return false;"><i class="fa fa-reply"></i> {$aLang.comment_repair}</a></li>
				{/if}
				
				{hook run='comment_action' comment=$oComment}
			{/if}
	
		</ul>
		
		
		<div id="comment_content_id_{$oComment->getId()}" class="border-radius comment-content text">
			{$oComment->getText()}
		</div>
			
	{else}				
		{$aLang.comment_was_delete}
	{/if}	
</section>
	{/foreach}	
</div>


{include file='paging.tpl' aPaging=$aPaging}