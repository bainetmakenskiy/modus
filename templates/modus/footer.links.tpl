      <div class="width-4">
            <div class="{if {cfg name='view.menu.logo'} == 'text'}logo{elseif {cfg name='view.menu.logo'} == 'images'}logo-img{/if}">
                <a href="{cfg name='path.root.web'}">
                  <span class="main">{cfg name='view.menu.logo_text1'}</span> 
                  <span class="text">{cfg name='view.menu.logo_text2'}</span>
                </a>
            </div>
            <p>{$sHtmlDescription}</p>
            <ul class="contact">
                <li>{$aLang.modus_footer_contact_phone}: <span>{cfg name='view.footer.phone'}</span></li>
                <li>{$aLang.modus_footer_contact_mail}: <span>{cfg name='view.footer.email'}</span></li>
            </ul>
        </div>
        <div class="width-2">
            <h6>{$aLang.modus_footer_info_title}</h6>
            <ul class="arrow-light">
                <li><a href="#">{$aLang.modus_footer_info_li_1}</a></li>
                <li><a href="#">{$aLang.modus_footer_info_li_2}</a></li>
                <li><a href="#">{$aLang.modus_footer_info_li_3}</a></li>
                <li><a href="#">{$aLang.modus_footer_info_li_4}</a></li>
            </ul>
        </div>
        <div class="width-2">
            <h6>{$aLang.modus_footer_community_title}</h6>
            <ul class="arrow-light">
                <li><a href="{router page='personal_blog'}">{$aLang.modus_footer_community_li_1}</a></li>
                <li><a href="{router page='blog'}">{$aLang.modus_footer_community_li_2}</a></li>
                <li><a href="{router page='people'}">{$aLang.modus_footer_community_li_3}</a></li>
                <li><a href="{router page='stream'}">{$aLang.modus_footer_community_li_4}</a></li>
            </ul>
        </div>
        <div class="width-4">
            <h5 class="footer-blog">{$aLang.modus_footer_topic_title} <span>{$aLang.modus_footer_topic_title_2}</span></h5>
            {assign var="aTopics" value=$LS->Topic_GetTopicsNew(1,{cfg name='view.footer.topic_n'})}
                {assign var="aTopics" value=$aTopics.collection}
                {foreach from=$aTopics item=oTopic}
                        {include file="topic_footer.tpl"}                         
                {/foreach}
        </div>