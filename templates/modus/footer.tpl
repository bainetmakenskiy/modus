			


			{hook run='content_end'}
		

<footer class="wrapper blue-light">
  <div class="wrapper liner-footer"></div>
    <div class="container">
    	{include file='footer.links.tpl'}
    </div>

    {hook run='footer_end'}
</footer>
<section class="wrapper blue">
    <div class="container copyright">
        <div class="align_left">{$aLang.modus_footer_contact_copyright} <a href="{cfg name='path.root.web'}">{cfg name='view.name'}</a>{if $sAction=='index'}, {hook run='copyright'}, {$aLang.modus_footer_contact_dev}{/if}</div>
        <div class="align_right">
            <ul>
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="{router page='rss'}"><i class="fa fa-rss"></i></a></li>
            </ul>
        </div>
    </div>
</section>

{include file='toolbar.tpl'}

{hook run='body_end'}

</body>
</html>