<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="ru"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="ru"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="ru"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="ru"> <!--<![endif]-->
<head>
	{hook run='html_head_begin'}

	<meta charset="utf-8">
	<title>{$sHtmlTitle}</title>
	<meta name="description" content="{$sHtmlDescription}">
	<meta name="keywords" content="{$sHtmlKeywords}">
	<meta name="author" content="Makenskiy Victor makenskiy.com html/css/responsive/cms">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	{$aHtmlHeadFiles.css}

	{if {cfg name='view.grid.type'} == 'mobile'}
		<link href="{cfg name='path.static.skin'}/css/mobile.css" rel="stylesheet">
	{/if}


  	<link rel="search" type="application/opensearchdescription+xml" href="{router page='search'}opensearch/" title="{cfg name='view.name'}" />

	{if $aHtmlRssAlternate}
		<link rel="alternate" type="application/rss+xml" href="{$aHtmlRssAlternate.url}" title="{$aHtmlRssAlternate.title}">
	{/if}

	{if $sHtmlCanonical}
		<link rel="canonical" href="{$sHtmlCanonical}" />
	{/if}
	
	{if $bRefreshToHome}
		<meta  HTTP-EQUIV="Refresh" CONTENT="3; URL={cfg name='path.root.web'}/">
	{/if}


	<!-- Поддержка html 5 тегов для IE -->
	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<link rel="shortcut icon" href="{cfg name='path.static.skin'}/images/favicon.ico?v1">
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="{cfg name='path.static.skin'}/images/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{cfg name='path.static.skin'}/images/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{cfg name='path.static.skin'}/images/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="{cfg name='path.static.skin'}/images/apple-touch-icon-57-precomposed.png">
	
	<script type="text/javascript">
		var DIR_WEB_ROOT 			= '{cfg name="path.root.web"}';
		var DIR_STATIC_SKIN 		= '{cfg name="path.static.skin"}';
		var DIR_ROOT_ENGINE_LIB 	= '{cfg name="path.root.engine_lib"}';
		var LIVESTREET_SECURITY_KEY = '{$LIVESTREET_SECURITY_KEY}';
		var SESSION_ID				= '{$_sPhpSessionId}';
		var BLOG_USE_TINYMCE		= '{cfg name="view.tinymce"}';
		
		var TINYMCE_LANG = 'en';
		{if $oConfig->GetValue('lang.current') == 'russian'}
			TINYMCE_LANG = 'ru';
		{/if}

		var aRouter = new Array();
		{foreach from=$aRouter key=sPage item=sPath}
			aRouter['{$sPage}'] = '{$sPath}';
		{/foreach}
	</script>
	
	
	{$aHtmlHeadFiles.js}

	
	<script type="text/javascript">
		var tinyMCE = false;
		ls.lang.load({json var = $aLangJs});
		ls.registry.set('comment_max_tree',{json var=$oConfig->Get('module.comment.max_tree')});
		ls.registry.set('block_stream_show_tip',{json var=$oConfig->Get('block.stream.show_tip')});
	</script>
	
	
	{hook run='html_head_end'}
</head>



{if $oUserCurrent}
	{assign var=body_classes value=$body_classes|cat:' ls-user-role-user'}
	
	{if $oUserCurrent->isAdministrator()}
		{assign var=body_classes value=$body_classes|cat:' ls-user-role-admin'}
	{/if}
{else}
	{assign var=body_classes value=$body_classes|cat:' ls-user-role-guest'}
{/if}

{if !$oUserCurrent or ($oUserCurrent and !$oUserCurrent->isAdministrator())}
	{assign var=body_classes value=$body_classes|cat:' ls-user-role-not-admin'}
{/if}

{add_block group='toolbar' name='toolbar_admin.tpl' priority=100}
{add_block group='toolbar' name='toolbar_scrollup.tpl' priority=-100}




<body>
	{hook run='body_begin'}
	
	
	{if $oUserCurrent}
		{include file='window_write.tpl'}
		{include file='window_favourite_form_tags.tpl'}
	{else}
		{include file='window_login.tpl'}
	{/if}
	

	{include file='header_top.tpl'}

		{include file='nav_content.tpl'}

		{if ($sAction=='index' and $sEvent=='newall') || ($sAction=='index' and $sEvent=='new') || ($sAction=='index' and $sEvent=='discussed') || ($sAction=='index' and $sEvent=='top') }
			{* Вырезаем слайдер топ с главной, оставляя только топики на страницах: newall, new, discussed, top *}
		{else}
			{if $sAction=='index' && {cfg name='view.topic.mainpage_topic_top'} == '1'}
           		{insert name="block" block=modusTopicsTop}
      		{/if}
      	{/if}

		{include file='system_message.tpl'}

	{if ($sAction=='index' and $sEvent=='newall') || ($sAction=='index' and $sEvent=='new') || ($sAction=='index' and $sEvent=='discussed') || ($sAction=='index' and $sEvent=='top') }
		{* Вырезаем слайдеры и прочее с главной, оставляя только топики на страницах: newall, new, discussed, top *}
	{else}
  		{if $sMenuItemSelect=='index'}
  			{if {cfg name='view.topic.mainpage_info_block'} == '1'}
				<section class="wrapper white info-block" role="banner">
					<div class="container info border-radius">
  						{hook run='header_banner_begin'}
  						<div class="text width-9">
    						<h3>{cfg name='view.name'}</h3>
    						<p>{cfg name='view.description'}</p>
  						</div>
  						<div class="width-3">
    						<a href="{cfg name='path.root.web'}" class="button align_right">{$aLang.modus_block_info_more}</a>
  						</div>
  						{hook run='header_banner_end'}
					</div>
				</section>
	{/if}
  
  		{if {cfg name='view.topic.mainpage_nav_2'} == '1'}
			{include file='nav.tpl'}
		{/if}

		{if {cfg name='view.topic.mainpage_about'} == '1'}
				{include file='about.tpl'}
		{/if}
	{/if}

  {/if}

  		{hook run='content_begin'}