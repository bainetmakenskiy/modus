<header class="wrapper blue-light">
  <div class="container">
    <div class="{if {cfg name='view.menu.logo'} == 'text'}logo{elseif {cfg name='view.menu.logo'} == 'images'}logo-img{/if} align_left">
      <a href="{cfg name='path.root.web'}">
        <span class="main">{cfg name='view.menu.logo_text1'}</span> 
        <span class="text">{cfg name='view.menu.logo_text2'}</span>
      </a>
    </div>
    <nav class="menu align_right">
    	{hook run='userbar_nav'}
        <ul>    
		  {if $iCountTopicsNew>0}<li><a href="{router page='index'}newall/" class="js-infobox{if $sMenuItemSelect=='index'} active{/if}" title="{$aLang.blog_menu_top_period_all}">{$aLang.blog_menu_all_new} +{$iCountTopicsNew}</a></li>{/if}
		  <li>
			<a href="{router page='blog'}" {if $sMenuItemSelect=='blog'}class="active"{/if}>{$aLang.blog_menu_collective} {if $iCountTopicsCollectiveNew>0}+{$iCountTopicsCollectiveNew}{/if}</a>
		  </li>
		  <li>
			<a href="{router page='personal_blog'}" {if $sMenuItemSelect=='log'}class="active"{/if}>{$aLang.blog_menu_personal}{if $iCountTopicsPersonalNew>0} +{$iCountTopicsPersonalNew}{/if}</a>
		  </li>
		  {hook run='menu_blog'}
          <li>
            <a href="{if $sMenuItemSelect=='index'}{router page='index'}top/{elseif $sMenuItemSelect=='blog'}{$sMenuSubBlogUrl}top/{elseif $sMenuItemSelect=='log'}{router page='personal_blog'}top/{/if}" class="bt-dropdown">
              <div class="txt">{$aLang.blog_menu_all_top}</div>
              <div class="ar"><i class="fa fa-chevron-down"></i></div>
            </a>
            <div class="dropdown">
                <div class="dropdown-line"></div>
                  <ul>
                  	{if $sAction=='index'}
                       <li {if $sPeriodSelectCurrent=='1'}class="active"{/if}><a href="{router page='index'}top?period=1">{$aLang.blog_menu_top_period_24h}</a></li>
                       <li {if $sPeriodSelectCurrent=='7'}class="active"{/if}><a href="{router page='index'}top?period=7">{$aLang.blog_menu_top_period_7d}</a></li>
                       <li {if $sPeriodSelectCurrent=='30'}class="active"{/if}><a href="{router page='index'}top?period=30">{$aLang.blog_menu_top_period_30d}</a></li>
                       <li {if $sPeriodSelectCurrent=='all'}class="active"{/if}><a href="{router page='index'}top?period=all">{$aLang.blog_menu_top_period_all}</a></li>
                    {else}
                  	   <li {if $sPeriodSelectCurrent=='1'}class="active"{/if}><a href="{$sPeriodSelectRoot}?period=1">{$aLang.blog_menu_top_period_24h}</a></li>
					             <li {if $sPeriodSelectCurrent=='7'}class="active"{/if}><a href="{$sPeriodSelectRoot}?period=7">{$aLang.blog_menu_top_period_7d}</a></li>
					             <li {if $sPeriodSelectCurrent=='30'}class="active"{/if}><a href="{$sPeriodSelectRoot}?period=30">{$aLang.blog_menu_top_period_30d}</a></li>
					             <li {if $sPeriodSelectCurrent=='all'}class="active"{/if}><a href="{$sPeriodSelectRoot}?period=all">{$aLang.blog_menu_top_period_all}</a></li>
                    {/if}
                  </ul>
            </div>
          </li>
          {if $oUserCurrent}

          {if {cfg name='view.menu.modal_write'} == '1'}
          	<li><a href="{router page='topic'}add/" id="modal_write_show">{$aLang.block_create}</a></li>
          {else}
            <li>
            	<a href="{router page='topic'}add/" class="bt-dropdown-add">
              		<div class="txt">{$aLang.block_create}</div>
              		<div class="ar"><i class="fa fa-chevron-down"></i></div>
            	</a>
            	<div class="dropdown-add">
                	<div class="dropdown-line"></div>
                  	<ul>
                  		<li {if $sMenuSubItemSelect=='topic'}class="active"{/if}><a href="{router page='topic'}add/">{$aLang.topic_menu_add_topic}</a></li>
						          <li {if $sMenuSubItemSelect=='question'}class="active"{/if}><a href="{router page='question'}add/">{$aLang.topic_menu_add_question}</a></li>
						          <li {if $sMenuSubItemSelect=='link'}class="active"{/if}><a href="{router page='link'}add/">{$aLang.topic_menu_add_link}</a></li>
						          <li {if $sMenuSubItemSelect=='photoset'}class="active"{/if}><a href="{router page='photoset'}add/">{$aLang.topic_menu_add_photoset}</a></li>
                      <li {if $sMenuSubItemSelect=='blog'}class="active"{/if}><a href="{router page='blog'}add/">{$aLang.block_create_blog}</a></li>
          
						{hook run='menu_create_topic_item'}
                  	</ul>
            	</div>
          	</li>
          {/if}
          	<li>
            	<a href="{$oUserCurrent->getUserWebPath()}" class="bt-dropdown-profile js-infobox" {if $iUserCurrentCountTalkNew} title="{$aLang.modus_menu_top_talk_title_1} {$iUserCurrentCountTalkNew} {$aLang.modus_menu_top_talk_title_2}"{/if}>
              		<div class="txt">{$oUserCurrent->getLogin()} {if $iUserCurrentCountTalkNew} ({$iUserCurrentCountTalkNew}){/if}</div>
              		<div class="ar"><i class="fa fa-chevron-down"></i></div>
            	</a>
            	<div class="dropdown-profile">
                	<div class="dropdown-line"></div>
                  	<ul>
                      <li><a href="{$oUserCurrent->getUserWebPath()}">{$aLang.user_menu_profile}</a></li>
                    	<li><a href="{$oUserCurrent->getUserWebPath()}favourites/topics/">{$aLang.user_menu_profile_favourites}</a></li>
                    	<li><a href="{router page='talk'}" id="new_messages" title="{if $iUserCurrentCountTalkNew}{$aLang.user_privat_messages_new}{/if}">{$aLang.user_privat_messages}{if $iUserCurrentCountTalkNew} ({$iUserCurrentCountTalkNew}){/if}</a></li>
                    	<li><a href="{router page='feed'}" {if $sMenuItemSelect=='feed'}class="active"{/if}>{$aLang.userfeed_title}</a></li>
                    	<li><a href="{router page='settings'}profile/">{$aLang.user_settings}</a></li>
                    	<li><a href="{router page='login'}exit/?security_ls_key={$LIVESTREET_SECURITY_KEY}">{$aLang.exit}</a></li>
                  	</ul>
            	</div>
          	</li>
          {else}
          	{hook run='userbar_item'}
			       <li><a href="{router page='login'}" class="js-login-form-show">{$aLang.user_login_submit}</a></li>
			       <li class="registration"><a href="{router page='registration'}" class="js-registration-form-show">{$aLang.registration_submit}</a></li>
          {/if}
          {hook run='main_menu_item'}
          <li><a href="{router page='search'}topics/" class="js-infobox" title="{$aLang.search}" class="search-top"><i class="fa fa-search"></i></a></li>
          
        </ul>
          {hook run='main_menu'}
          <a href="#" id="pull">Menu</a>
      </nav>
  </div>
  <div class="wrapper liner"></div>
</header>