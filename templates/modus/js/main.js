

/* ==================================
// простое меню
================================== */

// меню сворачивание
$(function() {
    var pull    =$('#pull');
        menu    =$('nav ul');
        menuHeight = menu.height();

    $(pull).on('click', function(e) {
        e.preventDefault();
        menu.slideToggle();
    });
});

// меню изменение размера дисплея
$(window).resize(function(){
    var w = $(window).width();
    if (w > 320 && menu.is(':hidden')) {
        menu.removeAttr('style');
    }
});

/* ==================================
// Выпадающие меню
================================== */

$(function() {

    var dropdown = $('.bt-dropdown');
    var menudropdown = $('.dropdown');
    
    $('ul li a', menudropdown).each(function() {
        $(this).append('<div />');
    });
    
    dropdown.toggle(function(e) {
        e.preventDefault();
        menudropdown.css({display: 'block'});
        $('.ar', this).html('<i class="fa fa-chevron-up"></i>').css({/*top: '9px'*/});
        $(this).addClass('active');
    },function() {
        menudropdown.css({display: 'none'});
        $('.ar', this).html('<i class="fa fa-chevron-down"></i>').css({/*top: '9px'*/});
        $(this).removeClass('active');
    });
        
});

$(function() {

    var dropdown = $('.bt-dropdown-add');
    var menudropdown = $('.dropdown-add');
    
    $('ul li a', menudropdown).each(function() {
        $(this).append('<div />');
    });
    
    dropdown.toggle(function(e) {
        e.preventDefault();
        menudropdown.css({display: 'block'});
        $('.ar', this).html('<i class="fa fa-chevron-up"></i>').css({/*top: '9px'*/});
        $(this).addClass('active');
    },function() {
        menudropdown.css({display: 'none'});
        $('.ar', this).html('<i class="fa fa-chevron-down"></i>').css({/*top: '9px'*/});
        $(this).removeClass('active');
    });
        
});

$(function() {

    var dropdown = $('.bt-dropdown-profile');
    var menudropdown = $('.dropdown-profile');
    
    $('ul li a', menudropdown).each(function() {
        $(this).append('<div />');
    });
    
    dropdown.toggle(function(e) {
        e.preventDefault();
        menudropdown.css({display: 'block'});
        $('.ar', this).html('<i class="fa fa-chevron-up"></i>').css({/*top: '9px'*/});
        $(this).addClass('active');
    },function() {
        menudropdown.css({display: 'none'});
        $('.ar', this).html('<i class="fa fa-chevron-down"></i>').css({/*top: '9px'*/});
        $(this).removeClass('active');
    });
        
});

$(function() {

    var dropdown = $('.bt-dropdown-pages');
    var menudropdown = $('.dropdown-pages');
    
    $('ul li a', menudropdown).each(function() {
        $(this).append('<div />');
    });
    
    dropdown.toggle(function(e) {
        e.preventDefault();
        menudropdown.css({display: 'block'});
        $('.ar', this).html('<i class="fa fa-chevron-up"></i>').css({/*top: '9px'*/});
        $(this).addClass('active');
    },function() {
        menudropdown.css({display: 'none'});
        $('.ar', this).html('<i class="fa fa-chevron-down"></i>').css({/*top: '9px'*/});
        $(this).removeClass('active');
    });
        
});

/* ==================================
// Nivo слайдер
================================== */
$(window).load(function() {
        $('#slider').nivoSlider();
        $('#slider_2').nivoSlider();
        $('#slider_3').nivoSlider();
        $('#slider_4').nivoSlider();
        $('#slider_5').nivoSlider();
        $('#slider_6').nivoSlider();
        $('#slider_7').nivoSlider();
        $('#slider_8').nivoSlider();
        $('#slider_9').nivoSlider();
        $('#slider_10').nivoSlider();
    });

/* ==================================
// Рогресс бар
================================== */
$(function() {
                $(".knob").knob();
                var val,up=0,down=0,i=0
                    ,$idir = $("div.idir")
                    ,$ival = $("div.ival")
                    ,incr = function() { i++; $idir.show().html("+").fadeOut(); $ival.html(i); }
                    ,decr = function() { i--; $idir.show().html("-").fadeOut(); $ival.html(i); };
                $("input.infinite").knob(
                                    {
                                    'min':0
                                    ,'max':20
                                    ,'stopper':false
                                    ,'change':function(v){
                                                if(val>v){
                                                    if(up){
                                                        decr();
                                                        up=0;
                                                    }else{up=1;down=0;}
                                                }else{
                                                    if(down){
                                                        incr();
                                                        down=0;
                                                    }else{down=1;up=0;}
                                                }
                                                val=v;
                                            }
                                    }
                                    );
            });

/* ==================================
// Карусель
================================== */
$(function() {

                //  Responsive layout, resizing the items
                $('#slider_index').carouFredSel({
                    responsive: true,
                    width: '100%',
                    auto: false,
                    prev: '#prev',
                    next: '#next',
                    pagination: "#pager",
                    scroll: 2,
                    items: {
                        width: 100,
                        height: '40%',  //  optionally resize item-height
                        visible: {
                            min: 2,
                            max: 6
                        }
                    }
                });

                //  Responsive layout, resizing the items
                $('#slider_social').carouFredSel({
                    height: 'auto',
                    prev: '#prev_social',
                    next: '#next_social',
                    auto: false,
                });


            });