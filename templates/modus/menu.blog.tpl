

{if $sMenuItemSelect!='index'}

		<section class="wrapper gray-light">
  			<div class="container sup-menu">
      			<div class="title align_left">
      				{if $sMenuItemSelect=='blog'}
        			<a href="{router page='blog'}">{$aLang.modus_menu_sup_collective}</a>
        			{elseif $sMenuItemSelect=='log'}
        			<a href="{router page='personal_blog'}">{$aLang.modus_menu_sup_personal}</a>
        			{elseif $sAction=='search'}
        			<a href="{router page='search'}">{$aLang.search}</a>
        			{/if}
      			</div>
      			<div class="nav align_right">

      				{if $sMenuItemSelect=='blog'}
        			<ul>
          				<li><a href="{$sMenuSubBlogUrl}" {if $sMenuSubItemSelect=='good'}class="active"{/if}>{$aLang.blog_menu_collective_good}</a> / </li>
						<li>
							<a href="{$sMenuSubBlogUrl}newall/" {if $sMenuSubItemSelect=='new'}class="active"{/if} title="{$aLang.blog_menu_top_period_all}">{$aLang.blog_menu_collective_new}{if $iCountTopicsBlogNew>0} +{$iCountTopicsBlogNew}{/if}</a> / 
						</li>
						<li><a href="{$sMenuSubBlogUrl}discussed/" {if $sMenuSubItemSelect=='discussed'}class="active"{/if}>{$aLang.blog_menu_collective_discussed}</a></li>
					{hook run='menu_blog_blog_item'}
					</ul>
        			{/if}

					{if $sMenuItemSelect=='log'}
					<ul>
						<li><a href="{router page='personal_blog'}" {if $sMenuSubItemSelect=='good'}class="active"{/if}>{$aLang.blog_menu_personal_good}</a> / </li>
						<li>
							<a href="{router page='personal_blog'}newall/" title="{$aLang.blog_menu_top_period_all}" {if $sMenuSubItemSelect=='new'}class="active"{/if}>{$aLang.blog_menu_personal_new} {if $iCountTopicsPersonalNew>0}+{$iCountTopicsPersonalNew}{/if}</a> / 
						</li>
						<li><a href="{router page='personal_blog'}discussed/" {if $sMenuSubItemSelect=='discussed'}class="active"{/if}>{$aLang.blog_menu_personal_discussed}</a></li>
					{hook run='menu_blog_log_item'}
					</ul>
					{/if}
        			
      			</div>
      			<div class="triangle-down"></div>
  			</div>
		</section>

{elseif ({cfg name='view.topic.mainpage_nav_2'} != '1' && $sMenuItemSelect=='index') || ($sAction=='index' and $sEvent=='newall') || ($sAction=='index' and $sEvent=='new') || ($sAction=='index' and $sEvent=='discussed') || ($sAction=='index' and $sEvent=='top')}
<section class="wrapper gray-light">
  			<div class="container sup-menu">
      			<div class="title align_left">	
        			<a href="{cfg name='path.root.web'}">{$aLang.modus_menu_sup_index}</a>
      			</div>
      			<div class="nav align_right">
        			<ul>
        				{if {cfg name='view.topic.mainpage_topic'} != '1'}
						<li><a href="{cfg name='path.root.web'}/" {if $sMenuSubItemSelect=='good'}class="active"{/if}>{$aLang.blog_menu_all_good}</a> / </li>
						{/if}
						<li>
							<a href="{router page='index'}newall/" class="js-infobox" title="{$aLang.blog_menu_top_period_all}" {if $sMenuSubItemSelect=='new'}class="active"{/if}>{$aLang.modus_blog_menu_all_new}</a>
				{if $iCountTopicsNew>0} / <a href="{router page='index'}new/" class="js-infobox" title="{$aLang.blog_menu_top_period_24h}">{$aLang.blog_menu_all_new} +{$iCountTopicsNew}</a>{/if} /
						</li>
						<li><a href="{router page='index'}discussed/" {if $sMenuSubItemSelect=='discussed'}class="active"{/if}>{$aLang.blog_menu_all_discussed}</a> / </li>
						<li><a href="{router page='index'}top/" class="js-infobox {if $sMenuSubItemSelect=='top'}active{/if}" title="{$aLang.modus_menu_top_title}">{$aLang.blog_menu_all_top}</a></li>
						{hook run='menu_blog_index_item'}
					</ul>
        			
      			</div>
      			<div class="triangle-down"></div>
  			</div>
		</section>
{/if}