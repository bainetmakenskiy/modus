		<section class="wrapper gray-light">
  			<div class="container sup-menu">
      			<div class="title align_left">
        			<span>{$aLang.blog_admin}:</span> <a href="{$oBlogEdit->getUrlFull()}">{$oBlogEdit->getTitle()|escape:'html'}</a>
      			</div>
      			<div class="nav align_right">

        			<ul>
        					<li><a href="{router page='blog'}edit/{$oBlogEdit->getId()}/" {if $sMenuItemSelect=='profile'}class="active"{/if}>{$aLang.blog_admin_profile}</a> / </li>
							<li><a href="{router page='blog'}admin/{$oBlogEdit->getId()}/" {if $sMenuItemSelect=='admin'}class="active"{/if}>{$aLang.blog_admin_users}</a></li>
					</ul>
        			
					{hook run='menu_blog_edit'}
      			</div>
      			<div class="triangle-down"></div>
  			</div>
		</section>
