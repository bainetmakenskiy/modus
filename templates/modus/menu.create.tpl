		<section class="wrapper gray-light">
  			<div class="container sup-menu">
      			<div class="title align_left">
        			<a href="{router page='topic'}add/">
              {if $sEvent=='add'}
                {$aLang.block_create}
        				{if $sMenuItemSelect=='topic'}
							     {$aLang.topic_menu_add}
						    {elseif $sMenuItemSelect=='blog'}
							     {$aLang.blog_menu_create}
						    {else}
							     {hook run='menu_create_item_select' sMenuItemSelect=$sMenuItemSelect}
						    {/if}
              {else}
                {$aLang.topic_topic_edit}
              {/if}
        			</a>
      			</div>
      			<div class="nav align_right">

        			<ul>
        					<li><a href="{router page='topic'}add/" {if $sMenuSubItemSelect=='topic'}class="active"{/if}>{$aLang.topic_menu_add_topic}</a> / </li>
        					<li><a href="{router page='question'}add/" {if $sMenuSubItemSelect=='question'}class="active"{/if}>{$aLang.topic_menu_add_question}</a> / </li>
        					<li><a href="{router page='link'}add/" {if $sMenuSubItemSelect=='link'}class="active"{/if}>{$aLang.topic_menu_add_link}</a> / </li>
        					<li><a href="{router page='photoset'}add/" {if $sMenuSubItemSelect=='photoset'}class="active"{/if}>{$aLang.topic_menu_add_photoset}</a> / </li>
        					<li><a href="{router page='blog'}add/" {if $sMenuSubItemSelect=='blog'}class="active"{/if}>{$aLang.blog_menu_create}</a></li>
        					{if $iUserCurrentCountTopicDraft}
								<li> / <a href="{router page='topic'}saved/" class="drafts">{$aLang.topic_menu_saved} ({$iUserCurrentCountTopicDraft})</a></li>
							{/if}
        					{hook run='menu_create_item' sMenuItemSelect=$sMenuItemSelect}
					</ul>
        			{hook run='menu_create' sMenuItemSelect=$sMenuItemSelect sMenuSubItemSelect=$sMenuSubItemSelect}

      			</div>
      			<div class="triangle-down"></div>
  			</div>
		</section>



