		<section class="wrapper gray-light">
  			<div class="container sup-menu">
      			<div class="title align_left">
        			<a href="{router page='people'}">{$aLang.user_list}</a>
      			</div>
      			<div class="nav align_right">

        			<ul>
        					<li><a href="{router page='people'}" {if $sMenuItemSelect=='all'}class="active"{/if}>{$aLang.people_menu_users_all}</a> / </li>
							<li><a href="{router page='people'}online/" {if $sMenuItemSelect=='online'}class="active"{/if}>{$aLang.people_menu_users_online}</a> / </li>
							<li><a href="{router page='people'}new/" {if $sMenuItemSelect=='new'}class="active"{/if}>{$aLang.people_menu_users_new}</a></li>
					
						{hook run='menu_people_people_item'}
					</ul>
        			
      			</div>
      			<div class="triangle-down"></div>
  			</div>
		</section>


{hook run='menu_people'}
