{hook run='profile_sidebar_menu_before' oUserProfile=$oUserProfile}

		<section class="wrapper gray-light">
  			<div class="container sup-menu">
      			<div class="title align_left">
        			<a href="{$oUserProfile->getUserWebPath()}" itemprop="nickname">{$aLang.modus_profile_munu_title}: {$oUserProfile->getLogin()}{if $oUserProfile->getProfileName()} <span itemprop="name" class="name">({$oUserProfile->getProfileName()|escape:'html'})</span>{/if}</a>
      			</div>
      			<div class="nav align_right">

        			<ul>
                {hook run='profile_sidebar_menu_item_first' oUserProfile=$oUserProfile}
        					<li><a href="{$oUserProfile->getUserWebPath()}" {if $sAction=='profile' && ($aParams[0]=='whois' or $aParams[0]=='')}class="active"{/if}>{$aLang.user_menu_profile_whois}</a> / </li>
                  <li><a href="{$oUserProfile->getUserWebPath()}wall/" {if $sAction=='profile' && $aParams[0]=='wall'}class="active"{/if}>{$aLang.user_menu_profile_wall}{if ($iCountWallUser)>0} ({$iCountWallUser}){/if}</a> / </li>
                  <li><a href="{$oUserProfile->getUserWebPath()}created/topics/" {if $sAction=='profile' && $aParams[0]=='created'}class="active"{/if}>{$aLang.user_menu_publication}{if ($iCountCreated)>0} ({$iCountCreated}){/if}</a> / </li>
                  <li><a href="{$oUserProfile->getUserWebPath()}friends/" {if $sAction=='profile' && $aParams[0]=='friends'}class="active"{/if}>{$aLang.user_menu_profile_friends}{if ($iCountFriendsUser)>0} ({$iCountFriendsUser}){/if}</a> / </li>
                  <li><a href="{$oUserProfile->getUserWebPath()}stream/" {if $sAction=='profile' && $aParams[0]=='stream'}class="active"{/if}>{$aLang.user_menu_profile_stream}</a> / </li>
                  {if $oUserCurrent and $oUserCurrent->getId() == $oUserProfile->getId()}
                    <li><a href="{router page='talk'}" {if $sAction=='talk'}class="active"{/if}>{$aLang.talk_menu_inbox}{if $iUserCurrentCountTalkNew} ({$iUserCurrentCountTalkNew}){/if}</a> {if $oUserCurrent and $oUserCurrent->getId() == $oUserProfile->getId()}/{/if} </li>
                    <li><a href="{router page='settings'}" {if $sAction=='settings'}class="active"{/if}>{$aLang.settings_menu}</a> </li>
                  {/if}
						    {hook run='profile_sidebar_menu_item_last' oUserProfile=$oUserProfile}
					    </ul>
        			
      			</div>
      			<div class="triangle-down"></div>
  			</div>
		</section>


{hook run='profile_sidebar_end' oUserProfile=$oUserProfile}
