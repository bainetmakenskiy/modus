		<section class="block blogs">
              <header>
                  <h5>{$aLang.user_menu_publication}</h5>
              </header>

              <ul class="arrow-circle">
              		<li>
						<a href="{$oUserProfile->getUserWebPath()}created/topics/">{$aLang.topic_title}  {if $iCountTopicUser} ({$iCountTopicUser}) {/if}</a>
					</li>
					<li>
						<a href="{$oUserProfile->getUserWebPath()}created/comments/">{$aLang.user_menu_publication_comment}  {if $iCountCommentUser} ({$iCountCommentUser}) {/if}</a>
					</li>
					{if $oUserCurrent and $oUserCurrent->getId()==$oUserProfile->getId()}
						<li>
							<a href="{$oUserProfile->getUserWebPath()}created/notes/">{$aLang.user_menu_profile_notes}  {if $iCountNoteUser} ({$iCountNoteUser}) {/if}</a>
						</li>
					{/if}
					{hook run='menu_profile_created_item' oUserProfile=$oUserProfile}
              </ul>
              {hook run='menu_profile_created' oUserProfile=$oUserProfile}
        </section>