		<section class="block blogs">
              <header>
                  <h5>{$aLang.user_menu_profile_favourites}</h5>
              </header>

              <ul class="arrow-circle">
					<li>
						<a href="{$oUserProfile->getUserWebPath()}favourites/topics/">{$aLang.user_menu_profile_favourites_topics}  {if $iCountTopicFavourite} ({$iCountTopicFavourite}) {/if}</a>
					</li>
					<li>
						<a href="{$oUserProfile->getUserWebPath()}favourites/comments/">{$aLang.user_menu_profile_favourites_comments}  {if $iCountCommentFavourite} ({$iCountCommentFavourite}) {/if}</a>
					</li>
					{hook run='menu_profile_favourite' oUserProfile=$oUserProfile}
              </ul>
              {hook run='menu_profile_created' oUserProfile=$oUserProfile}
        </section>


        {* Подключаем блок изюранных тегов *}
		{if $oUserCurrent and $oUserCurrent->getId()==$oUserProfile->getId()}
			{$aBlockParams.user=$oUserProfile}
				{insert name="block" block=tagsFavouriteTopic params=$aBlock.params}
		{/if}