		<section class="block blogs">
              <header>
                  <h5>{$aLang.settings_menu}</h5>
              </header>

              <ul class="arrow-circle">
					<li>
						<a href="{router page='settings'}profile/">{$aLang.settings_menu_profile}</a>
					</li>
					<li>
						<a href="{router page='settings'}account/">{$aLang.settings_menu_account}</a>
					</li>
					<li>
						<a href="{router page='settings'}tuning/">{$aLang.settings_menu_tuning}</a>
					</li>
					{if $oConfig->GetValue('general.reg.invite')}
						<li>
							<a href="{router page='settings'}invite/">{$aLang.settings_menu_invite}</a>
						</li>
					{/if}

					{hook run='menu_settings_settings_item'}
              </ul>
              {hook run='menu_settings'}
        </section>

		
		
		

