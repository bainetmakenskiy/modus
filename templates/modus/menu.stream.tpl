		<section class="wrapper gray-light">
  			<div class="container sup-menu">
      			<div class="title align_left">
      			{if $sAction=='feed'}
        			<a href="{router page='feed'}">{$aLang.modus_feed_menu}</a>
        		{else}
        			<a href="{router page='stream'}all/">{$aLang.stream_menu}</a>
        		{/if}
      			</div>
      			<div class="nav align_right">

        			<ul>
        			{if $oUserCurrent}
                		<li><a href="{router page='feed'}" {if $sAction=='feed'}class="active"{/if}>{$aLang.modus_feed_menu}</a> / </li>
                  		<li><a href="{router page='stream'}user/" {if $sMenuItemSelect=='user'}class="active"{/if}>{$aLang.stream_menu_user}</a> / </li>
                  	{/if}
                  		<li><a href="{router page='stream'}all/" {if $sMenuItemSelect=='all'}class="active"{/if}>{$aLang.stream_menu_all}</a></li>
                  		{hook run='menu_stream_item'}
					</ul>
        			{hook run='menu_stream'}

      			</div>
      			<div class="triangle-down"></div>
  			</div>
		</section>
