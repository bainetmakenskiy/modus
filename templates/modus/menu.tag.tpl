		<section class="wrapper gray-light">
  			<div class="container sup-menu">
      			<div class="title align_left">
        			<a href="{router page='people'}">{$aLang.topic_tags}</a>
      			</div>
      			<div class="nav align_right tag">

        			<form action="" method="GET" class="js-tag-search-form search-tags">
                  <input type="text" name="tag" placeholder="{$aLang.block_tags_search}" value="{$sTag|escape:'html'}" class="input-text input-width-4 autocomplete-tags js-tag-search" />
              </form>
        			
      			</div>
      			<div class="triangle-down"></div>
  			</div>
		</section>
