		<section class="wrapper gray-light">
  			<div class="container sup-menu">
      			<div class="title align_left">
        			<a href="{$oUserProfile->getUserWebPath()}" itemprop="nickname">{$aLang.modus_profile_munu_title}: {$oUserProfile->getLogin()}{if $oUserProfile->getProfileName()} <span itemprop="name" class="name">({$oUserProfile->getProfileName()|escape:'html'})</span>{/if}</a>
      			</div>
      			<div class="nav align_right">

        			<ul>
                
        					<li><a href="{router page='talk'}" {if $sMenuSubItemSelect=='inbox'}class="active"{/if}>{$aLang.talk_menu_inbox} {if $iUserCurrentCountTalkNew}({$iUserCurrentCountTalkNew}){/if}</a></li>
							{if $iUserCurrentCountTalkNew}
								<li><a href="{router page='talk'}inbox/new/" {if $sMenuSubItemSelect=='new'}class="active"{/if}>{$aLang.talk_menu_inbox_new}</a></li>
							{/if}
							<li><a href="{router page='talk'}add/" {if $sMenuSubItemSelect=='add'}class="active"{/if}>{$aLang.talk_menu_inbox_create}</a></li>
							<li><a href="{router page='talk'}favourites/" {if $sMenuSubItemSelect=='favourites'}class="active"{/if}>{$aLang.talk_menu_inbox_favourites}{if $iCountTalkFavourite} ({$iCountTalkFavourite}){/if}</a></li>
							<li><a href="{router page='talk'}blacklist/" {if $sMenuSubItemSelect=='blacklist'}class="active"{/if}>{$aLang.talk_menu_inbox_blacklist}</a></li>

							{hook run='menu_talk_talk_item'}
                  	
					    </ul>
        				{hook run='menu_talk'}

      			</div>
      			<div class="triangle-down"></div>
  			</div>
		</section>

