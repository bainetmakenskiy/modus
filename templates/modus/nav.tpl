{if $sMenuItemSelect=='index'}
<section class="wrapper white">
<div class="container nav-2-container">
  <div class="nav-2 width-3 border-radius">
          <i class="iconimg fa fa-group"></i>
          <i class="iconimg_h fa fa-group"></i>
          <p>{$aLang.modus_nav2_index_link_1}</p>
          <div class="pad text_align_center">
           {$aLang.modus_nav2_index_link_text_1} 
          </div>
          <a href="{router page='people'}" class="button">{$aLang.modus_nav2_index_link_more}</a>
          <a href="{router page='people'}" class="button_2">{$aLang.modus_nav2_index_link_more}</a>
  </div>
  <div class="nav-2 width-3 border-radius">
          <i class="iconimg fa fa-th-list"></i>
          <i class="iconimg_h fa fa-th-list"></i>
          <p>{$aLang.modus_nav2_index_link_2}</p>
          <div class="pad text_align_center">
           {$aLang.modus_nav2_index_link_text_2} 
          </div>
          <a href="{router page='stream'}" class="button">{$aLang.modus_nav2_index_link_more}</a>
          <a href="{router page='stream'}" class="button_2">{$aLang.modus_nav2_index_link_more}</a>
  </div>
  <div class="nav-2 width-3 border-radius">
          <i class="iconimg fa fa-book"></i>
          <i class="iconimg_h fa fa-book"></i>
          <p>{$aLang.modus_nav2_index_link_3}</p>
          <div class="pad text_align_center">
           {$aLang.modus_nav2_index_link_text_3} 
          </div>
          <a href="{router page='blogs'}" class="button">{$aLang.modus_nav2_index_link_more}</a>
          <a href="{router page='blogs'}" class="button_2">{$aLang.modus_nav2_index_link_more}</a>
  </div>
  <div class="nav-2 width-3 border-radius">
          <i class="iconimg fa fa-bar-chart-o"></i>
          <i class="iconimg_h fa fa-bar-chart-o"></i>
          <p>{$aLang.modus_nav2_index_link_4}</p>
          <div class="pad text_align_center">
           {$aLang.modus_nav2_index_link_text_4} 
          </div>
          <a href="{router page='index'}top/" class="button">{$aLang.modus_nav2_index_link_more}</a>
          <a href="{router page='index'}top/" class="button_2">{$aLang.modus_nav2_index_link_more}</a>
  </div>
</div>
</section>
<div class="wrapper liner-white"></div>
{/if}
