	{if $aPaging and $aPaging.iCountPage>1}
		<section class="pagination">
            <ul>
            	{if $aPaging.iPrevPage}
                	<li class="prew"><a href="{$aPaging.sBaseUrl}/{$aPaging.sGetParams}" class="js-paging-prev-page js-infobox" title="{$aLang.paging_previos}"><i class="fa fa-chevron-left"></i></a></li>
                {/if}

                {foreach from=$aPaging.aPagesLeft item=iPage}
					<li><a href="{$aPaging.sBaseUrl}/page{$iPage}/{$aPaging.sGetParams}">{$iPage}</a></li>
				{/foreach}

                <li><a href="#" class="active">{$aPaging.iCurrentPage}</a></li>
                
			
				{foreach from=$aPaging.aPagesRight item=iPage}
					<li><a href="{$aPaging.sBaseUrl}/page{$iPage}/{$aPaging.sGetParams}">{$iPage}</a></li>
				{/foreach}

                {if $aPaging.iNextPage}
                	<li class="next"><a href="{$aPaging.sBaseUrl}/page{$aPaging.iNextPage}/{$aPaging.sGetParams}" class="js-paging-next-page js-infobox" title="{$aLang.paging_next}"><i class="fa fa-chevron-right"></i></a></li>
                {/if}
            </ul>
        </section>
    {/if}