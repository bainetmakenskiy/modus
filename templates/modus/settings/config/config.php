<?php

$config = array();

/* Темы шаблона */
$config['view']['theme'] = 'default'; // default, green_orange, purpur_blue, light


/* Тип сетки:
 * 
 * mobile - адаптивный (1200px, 960px, 420px, 360px).
 * fixed - фиксированная ширина
 */
$config['view']['grid']['type'] = 'mobile';

// Логотип
$config['view']['menu']['logo'] = 'text'; 					// Текстовое - text, картинкой - images
$config['view']['menu']['logo_text1'] = 'modus'; 			// Большой текст логотипа
$config['view']['menu']['logo_text2'] = 'versus'; 			// Маленький текст логотипа

// Главное меню
$config['view']['menu']['modal_write'] = false; 			//  Вкл/выкл модальное окно "Создать". true - вкл, false - выкл.

// Настройка главной страницы.
$config['view']['topic']['mainpage_topic_top'] = true; 		// Вкл/выкл слайдер топ топиков. true - вкл, false - выкл.
$config['view']['topic']['mainpage_info_block'] = true; 	// Вкл/выкл блок информации (баннер) под слайдером топ топиков. true - вкл, false - выкл.
$config['view']['topic']['mainpage_nav_2'] = true; 			// Вкл/выкл меню nav-2, если выключен, появляется саб меню. true - вкл, false - выкл.
$config['view']['topic']['mainpage_about'] = true; 			// Вкл/выкл блок "О сайте". true - вкл, false - выкл.
$config['view']['topic']['mainpage_topic'] = true; 			// Вкл/выкл слайдер топиков. true - вкл, false - выкл.
$config['view']['topic']['mainpage_topic_type'] = 'good';   // Какие топики выводить в слайдер топиков. good - хорошие, new - новые, default - по умолчанию как в ls.
$config['view']['topic']['mainpage_n'] = 20; 				// Сколько топиков выводить в слайдер топиков.
$config['view']['topic']['slider_theme'] = 'new';			// Тема слайдера "Топ". new, bar, default, light, dark

// Прогрессбар на главной (Сделать вывод значений из деволтного конфига?)
$config['view']['progress']['n1'] = 2; 						// Первый, топик
$config['view']['progress']['n2'] = 10; 					// Второй, блог
$config['view']['progress']['n3'] = 2; 						// Третий, голосование
$config['view']['progress']['n4'] = 20;  					// Четвертый, бог

// Темы топиков
$config['view']['topic']['theme'] = 'default'; 				// default - стандартный, theme2 - 1 колонка аватар слева, theme3 - две колонки, theme4 - три колонки
$config['view']['topic']['theme_style'] = 'fourth'; 		// Вылетушки/эфекты для тем theme3 и theme4. Настройка: default, first, second, third, fourth, fifth, sixth, seventh, eighth, tenth
$config['view']['topic']['avatar_default'] = true; 			// Вкл/выкл аватары топиков в теме default. На странице добавления топика загрузка аватара не отключена, чтобы можно было в последствии менять темы вывода топиков.
$config['view']['topic']['photoset_theme'] = 'new';			// Тема слайдера фотосета. new, bar, default, light, dark

// Страница люди
$config['view']['people']['type'] = 'table'; 				// avatar - плиткой, table - таблицей
$config['view']['people']['theme_style'] = 'default'; 		// Вылетушки/эфекты для темы avatar. Настройка: default, first, second, third, fourth, fifth, sixth, seventh, eighth, tenth

// Страница блоги
$config['view']['blogs']['type'] = 'table'; 				// avatar - плиткой, table - таблицей
$config['view']['blogs']['theme_style'] = 'default'; 		// Вылетушки/эфекты для темы avatar. Настройка: default, first, second, third, fourth, fifth, sixth, seventh, eighth, tenth


// Сайдбар
$config['view']['sidebar']['blogs'] = 'list'; 				// Блок "Блоги". avatar - аватарками, list - списком

// Стиль таблиц, заголовок thead
$config['view']['table']['th'] = 'theme2'; 					// theme1, theme2, theme3

// Страница профиля
$config['view']['profile']['comments_list'] = false; 		// Вкл/выкл вывод комментариев

// Подвал
$config['view']['footer']['phone'] = '+7 xxx xxx xxxx'; 		// телефон для связи
$config['view']['footer']['email'] = 'info@your_site'; 		// Ваша почта для связи
$config['view']['footer']['topic_n'] = 2;					// Сколько новых топиков выводить в футере 

// Разное
$config['view']['login']['name'] = true;					// Вкл/выкл вывод логин автора или имя. Однако, в данном варианте, ЛС не ищет в поиске по имени, ищет только по логину.

// Изменяем дефолтные настройки LS
$config['module']['user']['avatar_size'] = array(260,100,64,48,24,0); 		// Список размеров аватаров у пользователя. 0 - исходный размер
$config['module']['blog']['avatar_size'] = array(260,100,70,64,48,24,0); 	// Список размеров аватаров у блога. 0 - исходный размер
$config['module']['user']['profile_photo_width'] = 420; 	  				// ширина квадрата фотографии в профиле, px.

/**
 * Фотосет. Список размеров превью, которые необходимо делать при загрузке фото. Если не знаете что это, не трогать.
 */

$config['module']['topic']['photoset']['size'] = array( 
	
	// Для слайдера топ
	array(
		'w' => 1200,
		'h' => 375,
		'crop' => true,
	),

	// Большая картинка
	array(
		'w' => 1200,
		'h' => null,
		'crop' => false,
	),

	// Для топиков в списке
	array(
		'w' => 770,
		'h' => 300,
		'crop' => true,
	),

	// Для топиков
	array(
		'w' => 870,
		'h' => 300,
		'crop' => true,
	),

	// Для топиков theme2
	array(
		'w' => 470,
		'h' => 270,
		'crop' => true,
	),

	// Для фотосета в редактировании
	array(
		'w' => 100,
		'h' => 100,
		'crop' => true,
	),

	// Маленькие в футер
	array(
		'w' => 70,
		'h' => 70,
		'crop' => true,
	)

	/* Дефолтные размеры в LS. В шаблоне не используются.
	array(
		'w' => 1000,
		'h' => null,
		'crop' => false,
	),
	array(
		'w' => 500,
		'h' => null,
		'crop' => false,
	),

	array(
		'w' => 100,
		'h' => 65,
		'crop' => true,
	),
	array(
		'w' => 50,
		'h' => 50,
		'crop' => true,
	)
	*/

);

$config['view']['img_resize_width'] = 1200;    // Нужно для автогенерации превью в топиках. До какого размера в пикселях ужимать картинку по щирине при загрузки её в топики и комменты

/**
 * Настройки подключения js. Если не знаете что это, не трогать.
 */

$config['head']['default']['js'] = Config::Get('head.default.js');
$config['head']['default']['js'][] = '___path.static.skin___/js/jquery.knob.js';
$config['head']['default']['js'][] = '___path.static.skin___/js/template.js';
$config['head']['default']['js'][] = '___path.static.skin___/js/main.js';
$config['head']['default']['js'][] = '___path.static.skin___/js/jquery.nivo.slider.js';
$config['head']['default']['js'][] = '___path.static.skin___/js/jquery.carouFredSel-6.2.1.js';
$config['head']['default']['js'][] = '___path.static.skin___/js/jquery.transit.min.js';
$config['head']['default']['js'][] = '___path.static.skin___/js/jquery.touchSwipe.min.js';
$config['head']['default']['js'][] = '___path.static.skin___/js/jquery.mousewheel.min.js';
$config['head']['default']['js'][] = '___path.static.skin___/js/jquery.ba-throttle-debounce.min.js';

/**
 * Настройки подключения css. Если не знаете что это, не трогать.
 */

$config['head']['default']['css'] = array(
	"___path.static.skin___/css/base.css",
	"___path.root.engine_lib___/external/jquery/markitup/skins/simple/style.css",
	"___path.root.engine_lib___/external/jquery/markitup/sets/default/style.css",
	"___path.root.engine_lib___/external/jquery/jcrop/jquery.Jcrop.css",
	"___path.root.engine_lib___/external/prettify/prettify.css",
	"___path.static.skin___/css/grid.css",
	"___path.static.skin___/css/nivo_slider/nivo-slider.css",
	"___path.static.skin___/css/nivo_slider/themes/bar/bar.css",
	"___path.static.skin___/css/nivo_slider/themes/dark/dark.css",
	"___path.static.skin___/css/nivo_slider/themes/default/default.css",
	"___path.static.skin___/css/nivo_slider/themes/light/light.css",
	"___path.static.skin___/css/nivo_slider/themes/new/new.css",
	"___path.static.skin___/css/fonts.css",
	"___path.static.skin___/css/tables.css",
	"___path.static.skin___/css/modals.css",
	"___path.static.skin___/css/infobox.css",
	"___path.static.skin___/css/jquery.notifier.css",
	"___path.static.skin___/css/smoothness/jquery-ui.css",
	"___path.static.skin___/themes/___view.theme___/style.css",
	"___path.static.skin___/css/print.css",
);

/**
 * Настройки вывода блоков.
 */
$config['block']['rule_index_blog'] = array(
	'action'  => array(
			'index', 'blog' => array('{topics}','{topic}','{blog}')
		),
	'blocks'  => array(
			'right' => array('stream'=>array('priority'=>50),'tags'=>array('priority'=>40),'blogs'=>array('params'=>array(),'priority'=>100))
		),
	'clear' => false,
);

$config['block']['rule_personal_blog'] = array(
	'action'  => array( 'personal_blog' ),
	'blocks'  => array( 'right' => array('blogs','stream','tags') ),
);

$config['block']['rule_blogs'] = array(
	'action'  => array( 'blogs' ),
	'blocks'  => array( 'right' => array('blogs','stream','tags') ),
);

return $config;
?>