<?php

/** 
 * English language file.
 */
return array(

// Top menu
    'modus_menu_top_title' => 'Sort for time. Press button top in top menu.',
    'modus_menu_top_talk_title_1' => 'You have',
    'modus_menu_top_talk_title_2' => 'massage',

// Sub menu

    'modus_menu_sup_collective' => "Collective blog`s",
    'modus_menu_sup_personal' => "Personal blog`s",
    'modus_menu_sup_index' => 'Main',
    'modus_menu_sup_plus' => 'plus',
    'modus_menu_sup_minus' => 'minus',
    'modus_blog_menu_all_new' => 'All new',
    'modus_feed_menu' => 'My Feed',

// Block information

    'modus_block_info_more' => 'Read more',

// Nav 2, index

    'modus_nav2_index_link_1' => 'People',
        'modus_nav2_index_link_text_1' => 'Text... Text... Text... Text... Text... Text... Text... Text... Text...',
    'modus_nav2_index_link_2' => 'Update site',
        'modus_nav2_index_link_text_2' => 'Text... Text... Text... Text... Text... Text... Text... Text... Text...',
    'modus_nav2_index_link_3' => 'Blogs',
        'modus_nav2_index_link_text_3' => 'Text... Text... Text... Text... Text... Text... Text... Text... Text...',
    'modus_nav2_index_link_4' => 'The best',
        'modus_nav2_index_link_text_4' => 'Text... Text... Text... Text... Text... Text... Text... Text... Text...',
    'modus_nav2_index_link_more' => 'Read more',

// Block about

    'modus_block_about_index_title' => 'Why?',
    'modus_block_about_index_title_text' => 'Text... Text... Text... Text... Text... Text... Text... Text... Text...',
    'modus_block_about_index_text_rating' => 'Text... Text... Text... Text... Text... Text... Text... Text... Text... Text... Text... Text... Text... Text... Text... Text... Text... Text...',
    'modus_block_about_index_title_rating_1' => 'Topic',
        'modus_block_about_index_title_tooltip_rating_1' => 'Text... Text... Text... Text... Text... Text... Text... Text... Text...',
    'modus_block_about_index_title_rating_2' => 'Blog',
        'modus_block_about_index_title_tooltip_rating_2' => 'Text... Text... Text... Text... Text... Text... Text... Text... Text...',
    'modus_block_about_index_title_rating_3' => 'Vote',
        'modus_block_about_index_title_tooltip_rating_3' => 'Text... Text... Text... Text... Text... Text... Text... Text... Text...',
    'modus_block_about_index_title_rating_4' => 'God',
        'modus_block_about_index_title_tooltip_rating_4' => 'Text... Text... Text... Text... Text... Text... Text... Text... Text...',
    'modus_block_about_index_function_title' => 'Function site?',
    'modus_block_about_index_function_li_1' => 'Create personal blog',
    'modus_block_about_index_function_li_2' => 'Create collective blog',
    'modus_block_about_index_function_li_3' => 'Massages',
    'modus_block_about_index_function_li_4' => 'Votes',
    'modus_block_about_index_function_li_5' => 'Create 4 type topics',
    'modus_block_about_index_function_li_6' => 'Comments',
    'modus_block_about_index_function_li_7' => 'More...',
    'modus_block_about_index_say_title' => 'People say?',
    'modus_block_about_index_say_1' => 'Text... Text... Text... Text... Text... Text... Text... Text... Text...',
    'modus_block_about_index_say_name_1' => 'Name',
    'modus_block_about_index_say_2' => 'Text... Text... Text... Text... Text... Text... Text... Text... Text...',
    'modus_block_about_index_say_name_2' => 'Name',


// Slider topics in the footer

    'modus_block_topic_index_slider_title' => 'The best topics',

// Footer

    'modus_footer_info_title' => 'Information',
    'modus_footer_info_li_1' => 'About As',
    'modus_footer_info_li_2' => 'FAQ',
    'modus_footer_info_li_3' => 'Contacts',
    'modus_footer_info_li_4' => 'Rules',
    'modus_footer_community_title' => 'Community',
    'modus_footer_community_li_1' => "Personal blog`s",
    'modus_footer_community_li_2' => "Collective blog`s",
    'modus_footer_community_li_3' => 'People',
    'modus_footer_community_li_4' => 'Stream',
    'modus_footer_topic_title' => 'New topics frome',
    'modus_footer_topic_title_2' => "blog`s",
    'modus_footer_contact_phone' => 'Phone',
        'modus_footer_contact_phone_1' => '+7 xxx xxx xxxx',
    'modus_footer_contact_mail' => 'E-mail',
        'modus_footer_contact_mail_1' => 'info@your_site',
    'modus_footer_contact_copyright' => '2014',
    'modus_footer_contact_dev' => 'Template by <a href="http://makenskiy.com">makenskiy</a>',

// Topics

    'modus_topic_panel_by' => 'Publish',
    'modus_topic_panel_by_in' => 'in',
    'modus_topic_panel_by_link' => 'Topic link:',
    'modus_topic_panel_by_question' => 'Topic question:',
    'modus_topic_panel_by_topic' => 'Topic:',
    'modus_topic_panel_by_photoset' => 'Topic photoset:',
    'modus_topic_question_vote_result_sort' => 'Sort',
    'modus_topic_share_title' => 'Share',
    'modus_topic_type_title_1' => 'Topic',
    'modus_topic_type_title_2' => 'Topic question',
    'modus_topic_type_title_3' => 'Topic photoset',
    'modus_topic_type_title_4' => 'Topic link',

// Sidebar

    'modus_sidebar_update' => 'Update comments',
    'modus_sidebar_blog_info_title' => 'Reading blog',

// Profile

    'modus_profile_munu_title' => 'Profile',
    'modus_profile_top_go_topic' => 'See all topics',
    'modus_profile_about_welcome' => 'Hellow!',
    'modus_profile_about_welcome_name' => 'My name is',
    'modus_profile_about_skill' => 'Skill vote.',
    'modus_profile_comments' => 'Comments',
    'modus_profile_note_title' => 'Note!',
    'modus_profile_note_title_1' => 'Note',
    'modus_profile_plus_title' => 'Vote',


// Tables

    'modus_table_sort_user' => 'Sort name',
    'modus_table_sort_date' => 'Sort date',
    'modus_table_sort_rating' => 'Sort rating',
    'modus_table_sort_skill' => 'Sort skill',

// Var
    'modus_no_data_1' => 'No data.',
    'modus_no_data_2' => 'No data.',
    'modus_no_data' => 'No data',
    'modus_add_favorite' => 'Favotite',

// Stantart text ls
    'block_blogs_all' => 'All',
    'block_stream' => 'New comments',
    'block_friends' => 'Change user',


);
