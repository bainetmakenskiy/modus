<?php

/**
 * Русский языковой файл.
 * Содержит текстовки шаблона.
 */
return array(

// Топ меню
    'modus_menu_top_title' => 'Для сортировки по времени, нажмите на кнопку топ в верхнем меню',
    'modus_menu_top_talk_title_1' => 'У вас',
    'modus_menu_top_talk_title_2' => 'сообщение',

// Саб меню

    'modus_menu_sup_collective' => 'Коллективные блоги',
    'modus_menu_sup_personal' => 'Персональные блоги',
    'modus_menu_sup_index' => 'Главная',
    'modus_menu_sup_plus' => 'плюс',
    'modus_menu_sup_minus' => 'минус',
    'modus_blog_menu_all_new' => 'Все новые',
    'modus_feed_menu' => 'Мой Feed',

// Блок с информацией

    'modus_block_info_more' => 'Подробнее',

// Навигация 2, на главной

    'modus_nav2_index_link_1' => 'Люди',
        'modus_nav2_index_link_text_1' => 'Это наше сообщество. Дабавляйте в друзья, следите, общяйтесь, голосуте.',
    'modus_nav2_index_link_2' => 'Активность',
        'modus_nav2_index_link_text_2' => 'Тут обновление всего сайта. Новые топики, комментарии, голосование, новые блоги.',
    'modus_nav2_index_link_3' => 'Блоги',
        'modus_nav2_index_link_text_3' => 'Это список коллективных блогов. Для того чтобы добавить топик в блог, его надо подключить.',
    'modus_nav2_index_link_4' => 'Топ',
        'modus_nav2_index_link_text_4' => 'Тут можно посмотреть список самых лучших топиков за 7 дней, которые попали на главную.',
    'modus_nav2_index_link_more' => 'читать далее',

// Блок о нас

    'modus_block_about_index_title' => 'почему мы?',
    'modus_block_about_index_title_text' => 'Какой то промо текст... Какой то промо текст...',
    'modus_block_about_index_text_rating' => 'Мы саморегулирующее соообество. Каждый пользователь может влиять на контент сайта путем голосования, за пользователя, комментарий, топик. Ниже представлен рейтинг доступа к функциям сайта.',
    'modus_block_about_index_title_rating_1' => 'Топик',
        'modus_block_about_index_title_tooltip_rating_1' => 'Рейтинг необходимый для публикации',
    'modus_block_about_index_title_rating_2' => 'Блог',
        'modus_block_about_index_title_tooltip_rating_2' => 'Рейтинг необходимый создания коллективного блога',
    'modus_block_about_index_title_rating_3' => 'Голосование',
        'modus_block_about_index_title_tooltip_rating_3' => 'Рейтинг необходимый для голосования за топики и пользователей',
    'modus_block_about_index_title_rating_4' => 'Бог',
        'modus_block_about_index_title_tooltip_rating_4' => 'Рейтинг снимающий ограничение на частоту: постинга, комментирования и отправки почты.',
    'modus_block_about_index_function_title' => 'Функции сайта? Держи!',
    'modus_block_about_index_function_li_1' => 'Создание личного блога',
    'modus_block_about_index_function_li_2' => 'Создание коллективного блога',
    'modus_block_about_index_function_li_3' => 'Внутренняя почта',
    'modus_block_about_index_function_li_4' => 'Голосование',
    'modus_block_about_index_function_li_5' => 'Создания 4 типов топиков',
    'modus_block_about_index_function_li_6' => 'Комментирование',
    'modus_block_about_index_function_li_7' => 'Еще...',
    'modus_block_about_index_say_title' => 'Отзывы? Не проблема!',
    'modus_block_about_index_say_1' => 'Какой-то текст отзыва... Какой-то текст отзыва... Какой-то текст отзыва...',
    'modus_block_about_index_say_name_1' => 'Имя Фамилия',
    'modus_block_about_index_say_2' => 'Какой-то текст отзыва... Какой-то текст отзыва... Какой-то текст отзыва...',
    'modus_block_about_index_say_name_2' => 'Имя Фамилия',


// Слайдер топиков в подвале

    'modus_block_topic_index_slider_title' => 'Лучшие топики',

// Подвал

    'modus_footer_info_title' => 'Информация',
    'modus_footer_info_li_1' => 'О нас',
    'modus_footer_info_li_2' => 'FAQ',
    'modus_footer_info_li_3' => 'Контакты',
    'modus_footer_info_li_4' => 'Правила',
    'modus_footer_community_title' => 'Сообщество',
    'modus_footer_community_li_1' => 'Персональные блоги',
    'modus_footer_community_li_2' => 'Коллективные блоги',
    'modus_footer_community_li_3' => 'Люди',
    'modus_footer_community_li_4' => 'Активность',
    'modus_footer_topic_title' => 'новые топики из',
    'modus_footer_topic_title_2' => 'блогов',
    'modus_footer_contact_phone' => 'Телефон',
        'modus_footer_contact_phone_1' => '8 991 256 9589',
    'modus_footer_contact_mail' => 'E-mail',
        'modus_footer_contact_mail_1' => 'info@your_site',
    'modus_footer_contact_copyright' => '2014',
    'modus_footer_contact_dev' => 'Template by <a href="http://makenskiy.com">makenskiy</a>',

// Топики

    'modus_topic_panel_by' => 'Опубликовал',
    'modus_topic_panel_by_in' => 'в',
    'modus_topic_panel_by_link' => 'Топик ссылку:',
    'modus_topic_panel_by_question' => 'Топик вопрос:',
    'modus_topic_panel_by_topic' => 'Топик:',
    'modus_topic_panel_by_photoset' => 'Топик фотосет:',
    'modus_topic_question_vote_result_sort' => 'Сортировка',
    'modus_topic_share_title' => 'Поделиться',
    'modus_topic_type_title_1' => 'Топик',
    'modus_topic_type_title_2' => 'Топик вопрос',
    'modus_topic_type_title_3' => 'Топик фотосет',
    'modus_topic_type_title_4' => 'Топик ссылка',

// Сайдбар

    'modus_sidebar_update' => 'Обновить комментарии',
    'modus_sidebar_blog_info_title' => 'Читать блог',

// Профиль

    'modus_profile_munu_title' => 'Профиль',
    'modus_profile_top_go_topic' => 'Показать все топки',
    'modus_profile_about_welcome' => 'Привет!',
    'modus_profile_about_welcome_name' => 'Меня зовут',
    'modus_profile_about_skill' => 'Сила голоса. Чтобы ее прокачать, нужно писать топики и комментарии.',
    'modus_profile_comments' => 'Комментарии',
    'modus_profile_note_title' => 'Заметка!',
    'modus_profile_note_title_1' => 'Заметка',
    'modus_profile_plus_title' => 'Проголосовать за пользователя.',


// Таблицы

    'modus_table_sort_user' => 'Сортировать по имени',
    'modus_table_sort_date' => 'Сортировать по дате',
    'modus_table_sort_rating' => 'Сортировать по рейтингу',
    'modus_table_sort_skill' => 'Сортировать по силе',

// Разное
    'modus_no_data_1' => 'О себе не заполненно.',
    'modus_no_data_2' => 'Контакты не заполненны.',
    'modus_no_data' => 'Нет данных',
    'modus_add_favorite' => 'В избранное',

// Переопределение стандартных
    'block_blogs_all' => 'Все',
    'block_stream' => 'Новые комментарии',
    'block_friends' => 'Выбрать получателей',

);

