{if $oUserCurrent and $oUserCurrent->isAdministrator()}
<section class="toolbar-admin">
	<a href="{router page='admin'}" class="js-title-comment" title="{$aLang.admin_title}">
		<i class="fa fa-gears"></i>
	</a>
</section>
{/if}