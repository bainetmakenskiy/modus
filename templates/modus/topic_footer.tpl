{assign var="oBlog" value=$oTopic->getBlog()}
{assign var="oUser" value=$oTopic->getUser()}
{assign var="oVote" value=$oTopic->getVote()}

            <div class="new-topic-blog clearfix">
                <div class="avatar">
                  {if $oTopic->getType()=='photoset' and $oTopic->getPreviewImage()}
                        <a href="{$oTopic->getUrl()}"><img class="img-rounded js-infobox" src="{$oTopic->getPreviewImageWebPath('70crop')}" alt="{$oTopic->getTitle()}" title="{$oTopic->getTitle()}" /></a>
                  {elseif $oTopic->getPreviewImage()}
                        <a href="{$oTopic->getUrl()}"><img class="img-rounded js-infobox" alt="{$oTopic->getTitle()}" title="{$oTopic->getTitle()}" src="{$oTopic->getPreviewImageWebPath('70crop')}"/></a>
                  {else}
                        <a href="{$oTopic->getUrl()}"><img class="img-rounded js-infobox" alt="{$oTopic->getTitle()}" title="{$oTopic->getTitle()}" src="{cfg name="path.static.skin"}/images/blog_avatar_70x70.png" /></a>
                  {/if}
                </div>
                <div class="text">
                    <p>{$oTopic->getTextShort()|strip_tags|truncate:90:'...'}</p>
                    <time>{date_format date=$oTopic->getDateAdd() format="d.m.Y"}</time>
                </div>
            </div>