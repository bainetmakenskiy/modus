{assign var="oBlog" value=$oTopic->getBlog()}
{assign var="oUser" value=$oTopic->getUser()}
{assign var="oVote" value=$oTopic->getVote()}
{assign var="oFavourite" value=$oTopic->getFavourite()}

{if $bTopicList}
      {include file='topic_part_header.tpl'}    
{else}

<!-- Для btopic добавляем класс btopic, а в topic-list убераем -->
          <section class="topic topic-skin-default topic-type-link btopic">
          
          
          	<article>
              {if !$bTopicList}
                    {include file='topic_part_header_btopic.tpl'}    
              {/if}

              <div class="text">
                {hook run='topic_content_begin' topic=$oTopic bTopicList=$bTopicList}
                {$oTopic->getText()}
                	{if $oTopic->getType() == 'link'}
						        <a href="{router page='link'}go/{$oTopic->getId()}/" class="link js-infobox" title="{$aLang.topic_link_count_jump}: {$oTopic->getLinkCountJump()}">{$oTopic->getLinkUrl()}</a>
					        {/if}
                {hook run='topic_content_end' topic=$oTopic bTopicList=$bTopicList}
              </div>

              
              {include file='topic_part_footer.tpl'}

          	</article>
          </section>

{/if}