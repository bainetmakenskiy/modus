{if count($aTopics)>0}
	{add_block group='toolbar' name='toolbar_topic.tpl' iCountTopic=count($aTopics)}

    {if ($sAction=='index' and $sEvent=='') and {cfg name='view.topic.mainpage_topic'} == '1'}
        <section class="wrapper">
    		<div class="container slider-carousel">

      			<div class="list_carousel responsive">

        			<div class="title">
            			<h6>{$aLang.modus_block_topic_index_slider_title}</h6>
        			</div>
        			<div id="pager" class="pager"></div>
        			<div class="line"></div>

        			<a id="prev" class="prev button" href="#"><i class="fa fa-chevron-right"></i></a>
        			<a id="next" class="next button" href="#"><i class="fa fa-chevron-left"></i></a>
            
            		<ul id="slider_index">
                   

                    {if {cfg name='view.topic.mainpage_topic_type'} == 'good'}

                        {assign var="aTopics" value=$LS->Topic_GetTopicsGood(1,{cfg name='view.topic.mainpage_n'})}
                        {assign var="aTopics" value=$aTopics.collection}
                        {foreach from=$aTopics item=oTopic}
                           {include file="topic_main.tpl"}                         
                        {/foreach}

                    {elseif {cfg name='view.topic.mainpage_topic_type'} == 'new'}

                        {assign var="aTopics" value=$LS->Topic_GetTopicsNew(1,{cfg name='view.topic.mainpage_n'})}
                        {assign var="aTopics" value=$aTopics.collection}
                        {foreach from=$aTopics item=oTopic}
                            {include file="topic_main.tpl"}                 
                        {/foreach}
                    {else}

                    {foreach from=$aTopics item=oTopic}
                        {if $LS->Topic_IsAllowTopicType($oTopic->getType())}
                            {assign var="sTopicTemplateName" value="topic_`$oTopic->getType()`.tpl"}
                            {include file="topic_main.tpl"}
                        {/if}
                    {/foreach}

                    {/if}

        			</ul>
      			</div>
    		</div>
		</section>
        
    {else}

        <section class="wrapper white">
            <div class="container"> 

                <section class="content {if $noSidebar}no-sidebar{/if} two-thirds">
	               {foreach from=$aTopics item=oTopic}
		              {if $LS->Topic_IsAllowTopicType($oTopic->getType())}
			             {assign var="sTopicTemplateName" value="topic_`$oTopic->getType()`.tpl"}
    	                 {include file=$sTopicTemplateName bTopicList=true}
		              {/if}
	               {/foreach}

                   {include file='paging.tpl' aPaging=$aPaging}
                </section>

                {if !$noSidebar && $sidebarPosition != 'left'}
                    {include file='sidebar.tpl'}
                {/if}

            </div>
        </section>
    {/if}
    
{else}
    <section class="wrapper white">
            <div class="container"> 

                <section class="content {if $noSidebar}no-sidebar{/if} two-thirds">
                    <div class="empty"> 
	                   {$aLang.blog_no_topic}
                    </div>
                </section>

                {if !$noSidebar && $sidebarPosition != 'left'}
                    {include file='sidebar.tpl'}
                {/if}

            </div>
        </section>
{/if}