{assign var="oBlog" value=$oTopic->getBlog()}
{assign var="oUser" value=$oTopic->getUser()}
{assign var="oVote" value=$oTopic->getVote()}

                        <li>
                              <a href="{$oTopic->getUrl()}">
                                    {if $oTopic->getType()=='photoset' and $oTopic->getPreviewImage()}
                                          <img class="full-with img-rounded js-infobox" src="{$oTopic->getPreviewImageWebPath('200crop')}" alt="{$oTopic->getTitle()}" title="{$oTopic->getTitle()}" />
                                    {elseif $oTopic->getPreviewImage()}
                                          <img class="full-with img-rounded js-infobox" alt="{$oTopic->getTitle()}" title="{$oTopic->getTitle()}" src="{$oTopic->getPreviewImageWebPath('200crop')}"/>
                                    {else}
                                          <img class="full-with img-rounded js-infobox" alt="{$oTopic->getTitle()}" title="{$oTopic->getTitle()}" src="{cfg name="path.static.skin"}/images/topic-avatar-top-200x150.png" />
                                    {/if}
                              </a>
                        </li>