{* При большом количестве закладок, переходим в слайдер *}

			  <footer class="slider-carousel">
                <div class="list_carousel responsive border-radius">
                  
                  <div class="share-text">
                    <span>{$aLang.modus_topic_share_title}</span>
                  </div>
                  <ul id="slider_social" class="social-share">
                    {hookb run="topic_share" topic=$oTopic bTopicList=$bTopicList}
                      <li><a href="#" class="facebook border-radius"><i class="fa fa-facebook"></i></a></li>
                      <li><a href="#" class="twitter border-radius"><i class="fa fa-twitter"></i></a></li>
                      <li><a href="#" class="google-plus border-radius"><i class="fa fa-google-plus"></i></a></li>
                      <li><a href="#" class="linkedin border-radius"><i class="fa fa-linkedin"></i></a></li>
                    {/hookb}
                  </ul>
                  <a id="prev_social" class="prev button" href="#"><i class="fa fa-chevron-right"></i></a>
                  <a id="next_social" class="next button" href="#"><i class="fa fa-chevron-left"></i></a>
                </div>
              </footer>
