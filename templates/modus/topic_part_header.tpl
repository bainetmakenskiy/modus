	{assign var="oBlog" value=$oTopic->getBlog()}
	{assign var="oUser" value=$oTopic->getUser()}
	{assign var="oVote" value=$oTopic->getVote()}
	{assign var="oFavourite" value=$oTopic->getFavourite()}

{* 
 * Тут вывод в topic-list с настройками вывода различных типов топиков из конфига. 
 *}

{if {cfg name='view.topic.theme'} == 'default'}

  {* Стандартный вывод топика *}
	<section class="topic topic-skin-default {if {cfg name='view.topic.avatar_default'} != 1}avatar-default{/if} js-topic">
          <div class="info">
              <div class="date border-radius text_align_center js-infobox" title="{date_format date=$oTopic->getDateAdd() format="j F Y, H:i"}">
                  <time>{date_format date=$oTopic->getDateAdd() format="F"}</time>
                  <div class="date-bg"></div>
                  <div class="date-bg-1"></div>
                  <time datetime="{date_format date=$oTopic->getDateAdd() format='c'}" class="namber">{date_format date=$oTopic->getDateAdd() format="j"}</time>
              </div>
              <div class="topic-type border-radius text_align_center js-infobox" title="{if $oTopic->getType() == 'topic'}
                        {$aLang.modus_topic_type_title_1}
                    {elseif $oTopic->getType() == 'question'}
                        {$aLang.modus_topic_type_title_2}
                    {elseif $oTopic->getType() == 'photoset'}
                        {$aLang.modus_topic_type_title_3}
                    {elseif $oTopic->getType() == 'link'}
                        {$aLang.modus_topic_type_title_4}
                    {/if}">
                  <i class="fa {if $oTopic->getType() == 'topic'}
                        fa-file-text-o
                    {elseif $oTopic->getType() == 'question'}
                        fa-question
                    {elseif $oTopic->getType() == 'photoset'}
                        fa-image
                    {elseif $oTopic->getType() == 'link'}
                        fa-link
                    {/if}"></i>
              </div>
          </div>
      
          <div class="topic-avatar clearfix">
          	{if $oTopic->getType() == 'photoset'}
                {assign var=oMainPhoto value=$oTopic->getPhotosetMainPhoto()}
                
                   	<div class="slider-wrapper {if {cfg name='view.topic.photoset_theme'} == 'new'}
                                                  theme-new
                                                {elseif {cfg name='view.topic.photoset_theme'} == 'bar'}
                                                  theme-bar
                                                {elseif {cfg name='view.topic.photoset_theme'} == 'default'}
                                                  theme-default
                                                {elseif {cfg name='view.topic.photoset_theme'} == 'light'}
                                                  theme-light
                                                {elseif {cfg name='view.topic.photoset_theme'} == 'dark'}
                                                  theme-dark
                                                {/if}
                                                  " id="topic-photo-images">
              			<div 
                    {assign var=x value= 1|rand:10 } 
                    {if $x == 1}
                      id="slider_2"
                    {elseif $x == 2} 
                      id="slider_3"
                    {elseif $x == 3}
                      id="slider_4"
                    {elseif $x == 4} 
                      id="slider_5"
                    {elseif $x == 5}
                      id="slider_6"
                    {elseif $x == 6} 
                      id="slider_7"
                    {elseif $x == 7}
                      id="slider_8"
                    {elseif $x == 8} 
                      id="slider_9"
                    {elseif $x == 9}
                      id="slider_10"
                    {else}
                      id="slider"
                    {/if}
                     class="nivoSlider">
                    {assign var=aPhotos value=$oTopic->getPhotosetPhotos(0, $oConfig->get('module.topic.photoset.per_page'))}
					         	{if count($aPhotos)}                                
						          {foreach from=$aPhotos item=oPhoto}
                          {if $noSidebar}
							               <a href="{$oPhoto->getWebPath(1200)}" class="photoset-image js-infobox" rel="[photoset]"  title="{$oPhoto->getDescription()}"><img src="{$oPhoto->getWebPath('1200crop')}" data-thumb="{$oPhoto->getWebPath('1200crop')}" data-transition="slideInLeft" alt="{$oPhoto->getDescription()}" title="{$oPhoto->getDescription()}" class="full-with" /></a>
                          {else}
                              <a href="{$oPhoto->getWebPath(1200)}" class="photoset-image js-infobox" rel="[photoset]"  title="{$oPhoto->getDescription()}"><img src="{$oPhoto->getWebPath('770crop')}" data-thumb="{$oPhoto->getWebPath('770crop')}" data-transition="slideInLeft" alt="{$oPhoto->getDescription()}" title="{$oPhoto->getDescription()}" class="full-with" /></a>
                          {/if}
							         {assign var=iLastPhotoId value=$oPhoto->getId()}
						          {/foreach}
					         	{/if}

				          <script type="text/javascript">
					           ls.photoset.idLast='{$iLastPhotoId}';
				          </script>
              		</div>
            </div>
            {else}
              {if {cfg name='view.topic.avatar_default'} == 1}
               	{if $oTopic->getType()=='photoset' and $oTopic->getPreviewImage()}
                    {if $noSidebar}
                      <a href="{$oTopic->getUrl()}"><img class="full-with img-rounded" src="{$oTopic->getPreviewImageWebPath('1200crop')}" alt="{$oTopic->getTitle()|escape:'html'}" /></a>
                    {else}
                      <a href="{$oTopic->getUrl()}"><img class="full-with img-rounded" src="{$oTopic->getPreviewImageWebPath('770crop')}" alt="{$oTopic->getTitle()|escape:'html'}" /></a>
                    {/if}
                {elseif $oTopic->getPreviewImage()}
                    {if $noSidebar}
                      <a href="{$oTopic->getUrl()}"><img class="full-with img-rounded" alt="{$oTopic->getTitle()|escape:'html'}" src="{$oTopic->getPreviewImageWebPath('770crop')}" /></a>
                    {else}
                      <a href="{$oTopic->getUrl()}"><img class="full-with img-rounded" alt="{$oTopic->getTitle()|escape:'html'}" src="{$oTopic->getPreviewImageWebPath('1200crop')}" /></a>
                    {/if}
				        {else}
                    {if $noSidebar}
                      <a href="{$oTopic->getUrl()}"><img class="full-with img-rounded" alt="{$oTopic->getTitle()|escape:'html'}" src="{cfg name="path.static.skin"}/images/slide.png" /></a>
                    {else}
                      <a href="{$oTopic->getUrl()}"><img class="full-with img-rounded" alt="{$oTopic->getTitle()|escape:'html'}" src="{cfg name="path.static.skin"}/images/topic_avatar.jpg" /></a>
                    {/if}
                {/if}
              {/if}
            {/if}
          </div>
      

          <article>
              <header>
                  <a href="{$oTopic->getUrl()}"><h2>{$oTopic->getTitle()|escape:'html'}</h2></a>
                  <ul class="panel clearfix">
                      <li>{$aLang.modus_topic_panel_by} 
                          <a href="{$oUser->getUserWebPath()}" {if $oUser->getProfileName() && {cfg name='view.login.name'} == '1'}class="js-infobox" title="{$oUser->getLogin()}"{/if}> 
                            {if {cfg name='view.login.name'} == '1'}
                                {if $oUser->getProfileName()}
                                  {$oUser->getProfileName()|escape:'html'}
                                {else}
                                  {$oUser->getLogin()}
                                {/if}
                            {else}
                                {$oUser->getLogin()}
                            {/if}
                          </a> 
                          {$aLang.modus_topic_panel_by_in} 
                          <a href="{$oBlog->getUrlFull()}">{$oBlog->getTitle()|escape:'html'}</a>
                      </li>
                      <li>
                      	<i class="fa fa-tags"></i>
                      	{strip}
							{if $oTopic->getTagsArray()}
								{foreach from=$oTopic->getTagsArray() item=sTag name=tags_list}
									{if !$smarty.foreach.tags_list.first}, {/if}<a rel="tag" class="tag" href="{router page='tag'}{$sTag|escape:'url'}/">{$sTag|escape:'html'}</a>
								{/foreach}
							{else}
									{$aLang.topic_tags_empty}
							{/if}
					  	{/strip}
                      </li>
                      <li>
                      	<i class="fa fa-comments"></i>
                      	<a href="{$oTopic->getUrl()}#comments" class="go-comments js-infobox" title="{$aLang.topic_comment_read}">
                      		{$oTopic->getCountComment()}
                      		{if $oTopic->getCountCommentNew()}<span class="new-comments green">+{$oTopic->getCountCommentNew()}</span>{/if}
                      	</a>
                  	  </li>
                  	  {hook run='topic_show_info' topic=$oTopic}
                  </ul>
              </header>

              <div class="text">
              {hook run='topic_content_begin' topic=$oTopic bTopicList=$bTopicList}
	
		    			 {if $bTopicList}
    						{$oTopic->getTextShort()}

						      {if $oTopic->getTextShort()!=$oTopic->getText()}
						        <a href="{$oTopic->getUrl()}" class="cut js-infobox" title="{$aLang.topic_read_more}">
							      {if $oTopic->getCutText()}
								        {$oTopic->getCutText()}
							      {else}
			         					{$aLang.topic_read_more}
      							{/if}
							         <i class="fa fa-arrow-right"></i>
						        </a>
						      {/if}
					     {else}
						      {$oTopic->getText()}
					     {/if}
	
				        {hook run='topic_content_end' topic=$oTopic bTopicList=$bTopicList}
              </div>
              {if !$bTopicList}
				          {hook run='topic_show_end' topic=$oTopic}
			        {/if}
          </article>
        </section>

{elseif {cfg name='view.topic.theme'} == 'theme2'}

{* Вывод топика в одну колонку *}
	<section class="topic topic-skin-theme2 js-topic">
          <div class="topic-avatar">
          	{if $oTopic->getType() == 'photoset'}
                {assign var=oMainPhoto value=$oTopic->getPhotosetMainPhoto()}
                   	<div class="slider-wrapper theme-new" id="topic-photo-images">
              			<div {assign var=x value= 1|rand:10 } 
                    {if $x == 1}
                      id="slider_2"
                    {elseif $x == 2} 
                      id="slider_3"
                    {elseif $x == 3}
                      id="slider_4"
                    {elseif $x == 4} 
                      id="slider_5"
                    {elseif $x == 5}
                      id="slider_6"
                    {elseif $x == 6} 
                      id="slider_7"
                    {elseif $x == 7}
                      id="slider_8"
                    {elseif $x == 8} 
                      id="slider_9"
                    {elseif $x == 9}
                      id="slider_10"
                    {else}
                      id="slider"
                    {/if} class="nivoSlider">         
                			{assign var=aPhotos value=$oTopic->getPhotosetPhotos(0, $oConfig->get('module.topic.photoset.per_page'))}
					         	{if count($aPhotos)}                                
						          {foreach from=$aPhotos item=oPhoto}
							           <a href="{$oPhoto->getWebPath(1000)}" class="photoset-image" rel="[photoset]"  title="{$oPhoto->getDescription()}"><img src="{$oPhoto->getWebPath('470crop')}" data-thumb="{$oPhoto->getWebPath('470crop')}" data-transition="slideInLeft" alt="{$oPhoto->getDescription()}" title="{$oPhoto->getDescription()}" class="full-with img-rounded" /></a>
							         {assign var=iLastPhotoId value=$oPhoto->getId()}
						          {/foreach}
					         	{/if}
				          <script type="text/javascript">
					           ls.photoset.idLast='{$iLastPhotoId}';
				          </script>
				    </div>
            </div>
            {else}
               	{if $oTopic->getType()=='photoset' and $oTopic->getPreviewImage()}
                    <a href="{$oTopic->getUrl()}"><img class="full-with img-rounded" src="{$oTopic->getPreviewImageWebPath('470crop')}" alt="{$oTopic->getTitle()|escape:'html'}" /></a>
                {elseif $oTopic->getPreviewImage()}
                    <a href="{$oTopic->getUrl()}"><img class="full-with img-rounded" alt="{$oTopic->getTitle()|escape:'html'}" src="{$oTopic->getPreviewImageWebPath('470crop')}" /></a>
				    {else}
                    <a href="{$oTopic->getUrl()}"><img class="full-with img-rounded" alt="{$oTopic->getTitle()|escape:'html'}" src="{cfg name="path.static.skin"}/images/topic_avatar_470x270.jpg" /></a>
                {/if}
            {/if}
          </div>

          <article>
              <header>
                  <a href="{$oTopic->getUrl()}"><h2>{$oTopic->getTitle()|escape:'html'|strip_tags|truncate:15:''}</h2></a>
              </header>

              <div class="text">
              	<div class="js-infobox" title="
															{if $oTopic->getType() == 'link'} 
																{$aLang.modus_topic_panel_by_link}
															{elseif $oTopic->getType() == 'question'}
																{$aLang.modus_topic_panel_by_question}
															{elseif $oTopic->getType() == 'photoset'}
																{$aLang.modus_topic_panel_by_photoset}
															{elseif $oTopic->getType() == 'topic'} 
																{$aLang.modus_topic_panel_by_topic}
															{/if} 
																{$aLang.modus_topic_panel_by} {$oUser->getLogin()} {$aLang.modus_topic_panel_by_in} {$oBlog->getTitle()|escape:'html'}, {date_format date=$oTopic->getDateAdd() format="j F Y, H:i"}, {$oTopic->getCountComment()} {$oTopic->getCountComment()|declension:$aLang.comment_declension:'russian'}">
                {$oTopic->getTextShort()|strip_tags|truncate:350:'...'}
                </div>
                <a href="{$oTopic->getUrl()}" class="button">{$aLang.topic_read_more}</a>
              </div>
          </article>
   </section>

{elseif {cfg name='view.topic.theme'} == 'theme3'}

{* Вывод топика в две колонки *}

	<section class="topic topic-skin-theme3 view {if {cfg name='view.topic.theme_style'} == 'default'}
	view-default
	{elseif {cfg name='view.topic.theme_style'} == 'first'}
	view-first
	{elseif {cfg name='view.topic.theme_style'} == 'second'}
	view-second
	{elseif {cfg name='view.topic.theme_style'} == 'third'}
	view-third
	{elseif {cfg name='view.topic.theme_style'} == 'fourth'}
	view-fourth
	{elseif {cfg name='view.topic.theme_style'} == 'fifth'}
	view-fifth
	{elseif {cfg name='view.topic.theme_style'} == 'sixth'}
	view-sixth
	{elseif {cfg name='view.topic.theme_style'} == 'seventh'}
	view-seventh
	{elseif {cfg name='view.topic.theme_style'} == 'eighth'}
	view-eighth
	{elseif {cfg name='view.topic.theme_style'} == 'tenth'}
	view-tenth
	{/if} js-topic">
          <div class="topic-avatar">
          	{if $oMainPhoto}
                {assign var=oMainPhoto value=$oTopic->getPhotosetMainPhoto()}
                   	<a href="{$oTopic->getUrl()}"><img class="full-with img-rounded" src="{$oMainPhoto->getWebPath(470)}" alt="{$oTopic->getTitle()|escape:'html'}" id="photoset-main-image-{$oTopic->getId()}" /></a>
            {else}
               	{if $oTopic->getType()=='photoset' and $oTopic->getPreviewImage()}
                    <a href="{$oTopic->getUrl()}"><img class="full-with img-rounded" src="{$oTopic->getPreviewImageWebPath('470crop')}" alt="{$oTopic->getTitle()|escape:'html'}" /></a>
                {elseif $oTopic->getPreviewImage()}
                    <a href="{$oTopic->getUrl()}"><img class="full-with img-rounded" alt="{$oTopic->getTitle()|escape:'html'}" src="{$oTopic->getPreviewImageWebPath('470crop')}" /></a>
				    {else}
                    <a href="{$oTopic->getUrl()}"><img class="full-with img-rounded" alt="{$oTopic->getTitle()|escape:'html'}" src="{cfg name="path.static.skin"}/images/topic_avatar_470x270.jpg" /></a>
                {/if}
            {/if}
          </div>
          <div class="mask">
              
              <p>{$oTopic->getTextShort()|strip_tags|truncate:150:'...'}</p>
              <div class="info">
                <a href="{$oTopic->getUrl()}#comments" class="border-radius js-infobox" title="{$oTopic->getCountComment()} {$oTopic->getCountComment()|declension:$aLang.comment_declension:'russian'}"><i class="fa fa-comments"></i></a>
                <a href="{$oTopic->getUrl()}" class="border-radius js-infobox" title="{$aLang.topic_read_more}"><i class="fa fa-link"></i></a>
              </div>
              <h2>{$oTopic->getTitle()|escape:'html'|strip_tags|truncate:45:'...'}</h2>
          </div>
    </section>


{elseif {cfg name='view.topic.theme'} == 'theme4'}
{* Вывод топика в три колонки *}
	<section class="topic topic-skin-theme4 view {if {cfg name='view.topic.theme_style'} == 'default'}
	view-default
	{elseif {cfg name='view.topic.theme_style'} == 'first'}
	view-first
	{elseif {cfg name='view.topic.theme_style'} == 'second'}
	view-second
	{elseif {cfg name='view.topic.theme_style'} == 'third'}
	view-third
	{elseif {cfg name='view.topic.theme_style'} == 'fourth'}
	view-fourth
	{elseif {cfg name='view.topic.theme_style'} == 'fifth'}
	view-fifth
	{elseif {cfg name='view.topic.theme_style'} == 'sixth'}
	view-sixth
	{elseif {cfg name='view.topic.theme_style'} == 'seventh'}
	view-seventh
	{elseif {cfg name='view.topic.theme_style'} == 'eighth'}
	view-eighth
	{elseif {cfg name='view.topic.theme_style'} == 'tenth'}
	view-tenth
	{/if} js-topic">
          <div class="topic-avatar">
          	{if $oMainPhoto}
                {assign var=oMainPhoto value=$oTopic->getPhotosetMainPhoto()}
                   	<a href="{$oTopic->getUrl()}"><img class="full-with img-rounded" src="{$oMainPhoto->getWebPath(260)}" alt="{$oTopic->getTitle()|escape:'html'}" id="photoset-main-image-{$oTopic->getId()}" /></a>
            {else}
               	{if $oTopic->getType()=='photoset' and $oTopic->getPreviewImage()}
                    <a href="{$oTopic->getUrl()}"><img class="full-with img-rounded" src="{$oTopic->getPreviewImageWebPath('260crop')}" alt="{$oTopic->getTitle()|escape:'html'}" /></a>
                {elseif $oTopic->getPreviewImage()}
                    <a href="{$oTopic->getUrl()}"><img class="full-with img-rounded" alt="{$oTopic->getTitle()|escape:'html'}" src="{$oTopic->getPreviewImageWebPath('260crop')}" /></a>
				    {else}
                    <a href="{$oTopic->getUrl()}"><img class="full-with img-rounded" alt="{$oTopic->getTitle()|escape:'html'}" src="{cfg name="path.static.skin"}/images/topic_avatar_260x270.jpg" /></a>
                {/if}
            {/if}
          </div>
          <div class="mask">
              
              <p>{$oTopic->getTextShort()|strip_tags|truncate:100:'...'}</p>
              <div class="info">
                <a href="{$oTopic->getUrl()}#comments" class="border-radius js-infobox" title="{$oTopic->getCountComment()} {$oTopic->getCountComment()|declension:$aLang.comment_declension:'russian'}"><i class="fa fa-comments"></i></a>
                <a href="{$oTopic->getUrl()}" class="border-radius js-infobox" title="{$aLang.topic_read_more}"><i class="fa fa-link"></i></a>
              </div>
              <h2>{$oTopic->getTitle()|escape:'html'|strip_tags|truncate:50:'...'}</h2>
          </div>
    </section>

{/if}