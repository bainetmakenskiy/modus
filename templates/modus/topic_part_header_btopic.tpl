<header>
                
                <div class="info">
                  <div class="topic-type border-radius text_align_center js-infobox" title="{if $oTopic->getType() == 'topic'}
                        {$aLang.modus_topic_type_title_1}
                    {elseif $oTopic->getType() == 'question'}
                        {$aLang.modus_topic_type_title_2}
                    {elseif $oTopic->getType() == 'photoset'}
                        {$aLang.modus_topic_type_title_3}
                    {elseif $oTopic->getType() == 'link'}
                        {$aLang.modus_topic_type_title_4}
                    {/if}">
                    <i class="fa 
                    {if $oTopic->getType() == 'topic'}
                        fa-file-text-o
                    {elseif $oTopic->getType() == 'question'}
                        fa-question
                    {elseif $oTopic->getType() == 'photoset'}
                        fa-image
                    {elseif $oTopic->getType() == 'link'}
                        fa-link
                    {/if}
                    "></i>
                  </div>
                </div>
                {if $oVote || ($oUserCurrent && $oTopic->getUserId() == $oUserCurrent->getId()) || strtotime($oTopic->getDateAdd()) < $smarty.now-$oConfig->GetValue('acl.vote.topic.limit_time')}
                  {assign var="bVoteInfoShow" value=true}
                {/if}
                <div class="topic-vote border-radius text_align_center js-infobox {if $bVoteInfoShow}js-infobox-vote-topic{/if}" id="vote_total_topic_{$oTopic->getId()}" title="{$aLang.topic_vote_count}: {$oTopic->getCountVote()}">
                    
                  {if $oVote}
                    <div class="vote-count">
                      {if $bVoteInfoShow}
                        {if $oTopic->getRating() > 0}+{/if}{$oTopic->getRating()}
                      {/if}
                    </div>
                  {elseif $oUserCurrent}
                    <a href="#" class="plus" onclick="return ls.vote.vote({$oTopic->getId()},this,1,'topic');"><i class="fa fa-thumbs-o-up"></i></a>
                    <a href="#" class="minus" onclick="return ls.vote.vote({$oTopic->getId()},this,-1,'topic');"><i class="fa fa-thumbs-o-down"></i></a>
                  {else}
                     <div class="vote-count">
                        {if $oTopic->getRating() > 0}+{/if}{$oTopic->getRating()}
                     </div>                 
                  {/if}
                </div>
                {if $bVoteInfoShow}
          <div id="vote-info-topic-{$oTopic->getId()}" style="display: none;">
            + {$oTopic->getCountVoteUp()}<br/>
            - {$oTopic->getCountVoteDown()}<br/>
            &nbsp; {$oTopic->getCountVoteAbstain()}<br/>
            {hook run='topic_show_vote_stats' topic=$oTopic}
          </div>
        {/if}
                  <h1>{$oTopic->getTitle()|escape:'html'}</h1>
                  <ul class="panel">
                      <li>{$aLang.modus_topic_panel_by} <a href="{$oUser->getUserWebPath()}" {if $oUser->getProfileName()}class="js-infobox" title="{$oUser->getLogin()}"{/if}>
                      {if {cfg name='view.login.name'} == '1'}
                          {if $oUser->getProfileName()}
                                {$oUser->getProfileName()|escape:'html'}
                          {else}
                                {$oUser->getLogin()}
                          {/if}
                      {else}
                                {$oUser->getLogin()}
                      {/if}
                      </a> {$aLang.modus_topic_panel_by_in} <a href="{$oBlog->getUrlFull()}">{$oBlog->getTitle()|escape:'html'}</a>, <time datetime="{date_format date=$oTopic->getDateAdd() format='c'}">{date_format date=$oTopic->getDateAdd() format="j F Y, H:i"}</time></li>
                      <li class="tags">
                        <i class="fa fa-tags"></i>
                        
                        {strip}
              {if $oTopic->getTagsArray()}
                {foreach from=$oTopic->getTagsArray() item=sTag name=tags_list}
                  {if !$smarty.foreach.tags_list.first}, {/if}<a rel="tag" class="tag" href="{router page='tag'}{$sTag|escape:'url'}/">{$sTag|escape:'html'}</a>
                {/foreach}
              {else}
                  {$aLang.topic_tags_empty}
              {/if}

              {if $oUserCurrent}
                
          
                    <li class="topic-tags-edit js-favourite-tag-edit" {if !$oFavourite}style="display:none;"{/if}>
                      <a href="#" onclick="return ls.favourite.showEditTags({$oTopic->getId()},'topic',this);" class="actions-edit"><i class="fa fa-pencil"></i>{$aLang.favourite_form_tags_button_show}</a>
                      {if $oFavourite}
                        {foreach from=$oFavourite->getTagsArray() item=sTag name=tags_list_user}
                          <span class="topic-tags-user js-favourite-tag-user">, <a rel="tag" href="{$oUserCurrent->getUserWebPath()}favourites/topics/tag/{$sTag|escape:'url'}/" class="tag">{$sTag|escape:'html'}</a></span>
                        {/foreach}
                      {/if}
                    </li>
                {/if}
              {/strip}
                      </li>
            {if $oUserCurrent and ($oUserCurrent->getId()==$oTopic->getUserId() or $oUserCurrent->isAdministrator() or $oBlog->getUserIsAdministrator() or $oBlog->getUserIsModerator() or $oBlog->getOwnerId()==$oUserCurrent->getId())}
            <li><a href="{cfg name='path.root.web'}/{$oTopic->getType()}/edit/{$oTopic->getId()}/" title="{$aLang.topic_edit}" class="actions-edit"><i class="fa fa-pencil"></i> {$aLang.topic_edit}</a></li>
            {/if}
        
            {if $oUserCurrent and ($oUserCurrent->isAdministrator() or $oBlog->getUserIsAdministrator() or $oBlog->getOwnerId()==$oUserCurrent->getId())}
            <li><a href="{router page='topic'}delete/{$oTopic->getId()}/?security_ls_key={$LIVESTREET_SECURITY_KEY}" title="{$aLang.topic_delete}" onclick="return confirm('{$aLang.topic_delete_confirm}');" class="actions-delete"><i class="fa fa-remove"></i> {$aLang.topic_delete}</a></li>
            {/if}
            <li class="topic-info-favourite">
                <a href="#" onclick="return ls.favourite.toggle({$oTopic->getId()},this,'topic');" class="favourite"><i class="fa {if $oUserCurrent && $oTopic->getIsFavourite()}fa-star{else}fa-star-o{/if}"></i> {$aLang.modus_add_favorite} <span class="favourite-count" id="fav_count_topic_{$oTopic->getId()}">({$oTopic->getCountFavourite()})</span></a>
            </li>

            {hook run='topic_show_info' topic=$oTopic}
                  </ul>
              </header>

          {if {cfg name='view.topic.avatar_default'} == 1}
              <div class="topic-avatar clearfix">

                  {if $oTopic->getPreviewImage() and $oTopic->getType()!='photoset'}
                      <img class="full-with img-rounded" alt="{$oTopic->getTitle()|escape:'html'}" src="{$oTopic->getPreviewImageWebPath('870crop')}" />
                  {elseif $oTopic->getType()!='photoset'}
                      <img class="full-with img-rounded" alt="{$oTopic->getTitle()|escape:'html'}" src="{cfg name="path.static.skin"}/images/topic_avatar_870x300.jpg" />
                  {/if}
             
              </div>
          {/if}