{assign var="oBlog" value=$oTopic->getBlog()}
{assign var="oUser" value=$oTopic->getUser()}
{assign var="oVote" value=$oTopic->getVote()}
{assign var="oFavourite" value=$oTopic->getFavourite()}


<script type="text/javascript">
	jQuery(window).load(function($) {
		ls.photoset.showMainPhoto({$oTopic->getId()});
	});
</script>
<script type="text/javascript" src="{cfg name='path.root.engine_lib'}/external/prettyPhoto/js/prettyPhoto.js"></script>	
<link rel='stylesheet' type='text/css' href="{cfg name='path.root.engine_lib'}/external/prettyPhoto/css/prettyPhoto.css" />
<script type="text/javascript">
		jQuery(document).ready(function($) {	
			$('.photoset-image').prettyPhoto({
				social_tools:'',
				show_title: false,
				slideshow:false,
				deeplinking: false
			});
		});
</script>

{if $bTopicList}
      {include file='topic_part_header.tpl'}    
{else}

<!-- Для btopic добавляем класс btopic, а в topic-list убераем -->
          <section class="topic topic-skin-default btopic">
          
          
          	<article>
              {if !$bTopicList}
                    {include file='topic_part_header_btopic.tpl'}    
              {/if}

              <div class="topic-avatar clearfix">
            	<div class="slider-wrapper theme-new" id="topic-photo-images">
              		<div id="slider" class="nivoSlider">         
                		{assign var=aPhotos value=$oTopic->getPhotosetPhotos(0, $oConfig->get('module.topic.photoset.per_page'))}
							{if count($aPhotos)}                                
								{foreach from=$aPhotos item=oPhoto}
									<a href="{$oPhoto->getWebPath(1200)}" class="photoset-image js-infobox" rel="[photoset]"  title="{$oPhoto->getDescription()}"><img src="{$oPhoto->getWebPath('870crop')}" data-thumb="{$oPhoto->getWebPath('870crop')}" data-transition="slideInLeft" alt="{$oPhoto->getDescription()}" alt="{$oPhoto->getDescription()}" class="full-with" /></a>
									{assign var=iLastPhotoId value=$oPhoto->getId()}
								{/foreach}
							{/if}
							<script type="text/javascript">
								ls.photoset.idLast='{$iLastPhotoId}';
							</script>
              		</div>
            	</div>
          	  </div>

              <div class="text">
              {hook run='topic_content_begin' topic=$oTopic bTopicList=$bTopicList}
                {$oTopic->getText()}
              {hook run='topic_content_end' topic=$oTopic bTopicList=$bTopicList}
              </div>

              
              {include file='topic_part_footer.tpl'}

          	</article>
          </section>

{/if}