<section class="wrapper white">
  <div class="container">     
      <section class="content two-thirds margin-off">

{if {cfg name='view.people.type'} == 'avatar'}

	{if $aUsersList}
			{foreach from=$aUsersList item=oUserList}
				{assign var="oSession" value=$oUserList->getSession()}
				{assign var="oUserNote" value=$oUserList->getUserNote()}

		
		<section class="topic topic-skin-theme4 view {if {cfg name='view.people.theme_style'} == 'default'}
	view-default
	{elseif {cfg name='view.people.theme_style'} == 'first'}
	view-first
	{elseif {cfg name='view.people.theme_style'} == 'second'}
	view-second
	{elseif {cfg name='view.people.theme_style'} == 'third'}
	view-third
	{elseif {cfg name='view.people.theme_style'} == 'fourth'}
	view-fourth
	{elseif {cfg name='view.people.theme_style'} == 'fifth'}
	view-fifth
	{elseif {cfg name='view.people.theme_style'} == 'sixth'}
	view-sixth
	{elseif {cfg name='view.people.theme_style'} == 'seventh'}
	view-seventh
	{elseif {cfg name='view.people.theme_style'} == 'eighth'}
	view-eighth
	{elseif {cfg name='view.people.theme_style'} == 'tenth'}
	view-tenth
	{/if}">
          <div class="topic-avatar">
            <img src="{$oUserList->getProfileAvatarPath(260)}" class="full-with img-rounded" alt="{$oUserList->getLogin()}" />
          </div>
          <div class="mask">
              
              {if $oUserList->getProfileAbout()}
                    <p>{$oUserList->getProfileAbout()|strip_tags|truncate:130:'...'}</p>
              {else}
              		<p>{$aLang.modus_no_data_1}</p>
              {/if}
              <div class="info">
                <a href="{$oUserList->getUserWebPath()}" class="border-radius"><i class="fa fa-link"></i></a>
              </div>
              {if $oUserList->getProfileName()}
                    <h2>{$oUserList->getProfileName()|escape:'html'}</h2>
              {else}
                    <h2>{$oUserList->getLogin()}</h2>
              {/if}
              
          </div>
		</section>

			{/foreach}
	{else}
				<p>
					{if $sUserListEmpty}
						{$sUserListEmpty}
					{else}
						{$aLang.user_empty}
					{/if}
				</p>
	{/if}

{elseif {cfg name='view.people.type'} == 'table'}

<table class="table table-users">
	{if $bUsersUseOrder}
		<!-- thead class="theme1:theme2:theme3" -->
		<thead class="{if {cfg name='view.table.th'} == 'theme1'}
						theme1
					  {elseif {cfg name='view.table.th'} == 'theme2'}
					  	theme2
					  {elseif {cfg name='view.table.th'} == 'theme3'}
					  	theme3
					  {/if}">
			<tr>
				<th class="cell-name js-infobox" title="{$aLang.modus_table_sort_user}"><a href="{$sUsersRootPage}?order=user_login&order_way={if $sUsersOrder=='user_login'}{$sUsersOrderWayNext}{else}{$sUsersOrderWay}{/if}" {if $sUsersOrder=='user_login'}class="{$sUsersOrderWay}"{/if}>{$aLang.user}</a></th>
				<th>{$aLang.user_date_last}</th>
				<th class="js-infobox" title="{$aLang.modus_table_sort_date}"><a href="{$sUsersRootPage}?order=user_date_register&order_way={if $sUsersOrder=='user_date_register'}{$sUsersOrderWayNext}{else}{$sUsersOrderWay}{/if}" {if $sUsersOrder=='user_date_register'}class="{$sUsersOrderWay}"{/if}>{$aLang.user_date_registration}</a></th>
				<th class="cell-skill js-infobox" title="{$aLang.modus_table_sort_skill}"><a href="{$sUsersRootPage}?order=user_skill&order_way={if $sUsersOrder=='user_skill'}{$sUsersOrderWayNext}{else}{$sUsersOrderWay}{/if}" {if $sUsersOrder=='user_skill'}class="{$sUsersOrderWay}"{/if}>{$aLang.user_skill}</a></th>
				<th class="cell-rating js-infobox" title="{$aLang.modus_table_sort_rating}"><a href="{$sUsersRootPage}?order=user_rating&order_way={if $sUsersOrder=='user_rating'}{$sUsersOrderWayNext}{else}{$sUsersOrderWay}{/if}" {if $sUsersOrder=='user_rating'}class="{$sUsersOrderWay}"{/if}>{$aLang.user_rating}</a></th>
			</tr>
		</thead>
	{else}
		<thead class="{if {cfg name='view.table.th'} == 'theme1'}
						theme1
					  {elseif {cfg name='view.table.th'} == 'theme2'}
					  	theme2
					  {elseif {cfg name='view.table.th'} == 'theme3'}
					  	theme3
					  {/if}">
			<tr>
				<th class="cell-name">{$aLang.user}</th>
				<th class="cell-date">{$aLang.user_date_last}</th>
				<th class="cell-date">{$aLang.user_date_registration}</th>
				<th class="cell-skill">{$aLang.user_skill}</th>
				<th class="cell-rating">{$aLang.user_rating}</th>
			</tr>
		</thead>
	{/if}

	<tbody>
		{if $aUsersList}
			{foreach from=$aUsersList item=oUserList}
				{assign var="oSession" value=$oUserList->getSession()}
				{assign var="oUserNote" value=$oUserList->getUserNote()}
				<tr>
					<td class="cell-name">
						<a href="{$oUserList->getUserWebPath()}"><img src="{$oUserList->getProfileAvatarPath(24)}" alt="{$oUserList->getLogin()}" class="avatar img-rounded" /></a>
						<p class="username word-wrap">
							<a href="{$oUserList->getUserWebPath()}" class="js-infobox" title="{if $oUserList->getProfileName()}{$oUserList->getProfileName()|escape:'html'}{/if}">{$oUserList->getLogin()}</a>
							{if $oUserNote}
								<i class="icon-comment js-infobox" title="{$oUserNote->getText()|escape:'html'}"></i>
							{/if}
						</p>
					</td>
					<td class="cell-date">{if $oSession}{date_format date=$oSession->getDateLast() format="d.m.y, H:i"}{/if}</td>
					<td class="cell-date">{date_format date=$oUserList->getDateRegister() format="d.m.y, H:i"}</td>
					<td class="cell-skill">{$oUserList->getSkill()}</td>
					<td class="cell-rating"><strong>{$oUserList->getRating()}</strong></td>
				</tr>
			{/foreach}
		{else}
			<tr>
				<td colspan="5">
					{if $sUserListEmpty}
						{$sUserListEmpty}
					{else}
						{$aLang.user_empty}
					{/if}
				</td>
			</tr>
		{/if}
	</tbody>
</table>

{/if}

{include file='paging.tpl' aPaging=$aPaging}

		</section>

		{if !$noSidebar && $sidebarPosition != 'left'}
                    {include file='sidebar.tpl'}
                {/if}

    </div>
</section>